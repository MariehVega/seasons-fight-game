class DestroyableBox {
    constructor(mesh, boundingRadius) {
        this.mesh = mesh;
        this.collidableRadius = boundingRadius;
    }

    destroy(normal, callback) {
        let destruibleRay = new THREE.Raycaster();
        destruibleRay.ray.direction.set(normal.x, normal.y, normal.z);

        let origin = this.mesh.position.clone();//la de la bomba
        destruibleRay.ray.origin.copy(origin);

        let intersections = destruibleRay.intersectObjects(destruibleList);

        if (intersections.length > 0) {
            let distance = intersections[0].distance;
            if (distance <= this.collidableRadius - 0.5) {
                callback(intersections[0]);
            }
        }

    }

    destroyLeft() {
        let callback = (cosa) => {
            scene.remove(cosa.object);
            var indexColli = collidableList.indexOf(cosa.object);
            var indexDestru = destruibleList.indexOf(cosa.object);
            if (indexColli<0) { //Quiere decir que es un jugador, porque no son colisionables
                for (let i = 0; i < players.length; i++) {
                    if (players[i].element == cosa.object) {
                        players.splice(i, 1);
                        playersMesh.splice(i, 1);
                    }
                }
                destruibleList.splice(indexDestru, 1);
            }else{
                collidableList.splice(indexColli, 1);
                destruibleList.splice(indexDestru, 1);
            }
        }
        this.destroy({ x: -1, y: 0, z: 0 }, callback);
    }

    destroyRight() {
        let callback = (cosa) => {
            scene.remove(cosa.object);
            var indexColli = collidableList.indexOf(cosa.object);
            var indexDestru = destruibleList.indexOf(cosa.object);
            if (indexColli<0) { //Quiere decir que es un jugador, porque no son colisionables
                for (let i = 0; i < players.length; i++) {
                    if (players[i].element == cosa.object) {
                        players.splice(i, 1);
                        playersMesh.splice(i, 1);
                    }
                }
                destruibleList.splice(indexDestru, 1);
            }else{
                collidableList.splice(indexColli, 1);
                destruibleList.splice(indexDestru, 1);
            }
        }
        this.destroy({ x: 1, y: 0, z: 0 }, callback);
    }

    destroyUp() {
        let callback = (cosa) => {
            scene.remove(cosa.object);
            var indexColli = collidableList.indexOf(cosa.object);
            var indexDestru = destruibleList.indexOf(cosa.object);
            if (indexColli<0) { //Quiere decir que es un jugador, porque no son colisionables
                for (let i = 0; i < players.length; i++) {
                    if (players[i].element == cosa.object) {
                        players.splice(i, 1);
                        playersMesh.splice(i, 1);
                    }
                }
                destruibleList.splice(indexDestru, 1);
            }else{
                collidableList.splice(indexColli, 1);
                destruibleList.splice(indexDestru, 1);
            }
        }
        this.destroy({ x: 0, y: 0, z: -1 }, callback);
    }

    destroyDown() {
        let callback = (cosa) => {
            scene.remove(cosa.object);
            var indexColli = collidableList.indexOf(cosa.object);
            var indexDestru = destruibleList.indexOf(cosa.object);
            if (indexColli<0) { //Quiere decir que es un jugador, porque no son colisionables
                for (let i = 0; i < players.length; i++) {
                    if (players[i].element == cosa.object) {
                        players.splice(i, 1);
                        playersMesh.splice(i, 1);
                    }
                }
                destruibleList.splice(indexDestru, 1);
            }else{
                collidableList.splice(indexColli, 1);
                destruibleList.splice(indexDestru, 1);
            }
        }
        this.destroy({ x: 0, y: 0, z: 1 }, callback);
    }

    update() {
        this.destroyLeft();
        this.destroyRight();
        this.destroyUp();
        this.destroyDown();
    }
}