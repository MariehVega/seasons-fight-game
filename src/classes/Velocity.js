class Velocity {
    constructor(mesh, boundingRadius) {
        this.collidableRadius = boundingRadius;
        this.mesh = mesh;
    }

    set element(mesh) {
        if (mesh instanceof THREE.Mesh) {
            this._element = mesh;
        } else {
            let geometry = new THREE.BoxGeometry(100, 100, 100);
            let material = new THREE.MeshPhongMaterial({ map: THREE.ImageUtils.loadTexture('assets/textures/TexturasVelocidadInv.png'), transparent: true })
            this._element = new THREE.Mesh(geometry, material);
            this._element.castShadow = true;
            this._element.receiveShadow = true;
            this._element.position.set(this.x, this.y, this.z)
        }
    }

    get element() {
        return this._element;
    }

    collide(normal, callback) {
        let collidableRay = new THREE.Raycaster();
        collidableRay.ray.direction.set(normal.x, normal.y, normal.z);

        let origin = this.mesh.position.clone();
        collidableRay.ray.origin.copy(origin);

        let intersections = collidableRay.intersectObjects(playersMesh);

        if (intersections.length > 0) {
            let distance = intersections[0].distance;
            if (distance <= this.collidableRadius-0.5) {
                callback(intersections[0]);
            }
        }
    }

    collideLeft() {
        let callback = (cosa) => {
            scene.remove(this.mesh);
            var indexMesh = PUVelocityList.indexOf(this.mesh);
            PUVelocityList.splice(indexMesh, 1);
            var index = playersMesh.indexOf(cosa.object);
            players[index].control.vx = 15;
            var count = 10, counter;
            counter = setInterval(() => {
                if (count == 0) {
                    clearInterval(counter);
                    players[index].control.vx = 5.5;
                }
                count--;
            }, 1000);
        }
        this.collide({ x: -1, y: 0, z: 0 }, callback);
    }

    collideRight() {
        let callback = (cosa) => {
            scene.remove(this.mesh);
            var indexMesh = PUVelocityList.indexOf(this.mesh);
            PUVelocityList.splice(indexMesh, 1);
            var index = playersMesh.indexOf(cosa.object);
            players[index].control.vx = 15;
            var count = 10, counter;
            counter = setInterval(() => {
                if (count == 0) {
                    clearInterval(counter);
                    players[index].control.vx = 5.5;
                }
                count--;
            }, 1000);
        }
        this.collide({ x: 1, y: 0, z: 0 }, callback);
    }

    collideUp() {
        let callback = (cosa) => {
            scene.remove(this.mesh);
            var indexMesh = PUVelocityList.indexOf(this.mesh);
            PUVelocityList.splice(indexMesh, 1);
            var index = playersMesh.indexOf(cosa.object);
            players[index].control.vx = 15;
            var count = 10, counter;
            counter = setInterval(() => {
                if (count == 0) {
                    clearInterval(counter);
                    players[index].control.vx = 5.5;
                }
                count--;
            }, 1000);
        }
        this.collide({ x: 0, y: 0, z: -1 }, callback);
    }

    collideDown() {
        let callback = (cosa) => {
            scene.remove(this.mesh);
            var indexMesh = PUVelocityList.indexOf(this.mesh);
            PUVelocityList.splice(indexMesh, 1);
            var index = playersMesh.indexOf(cosa.object);
            players[index].control.vx = 15;
            var count = 10, counter;
            counter = setInterval(() => {
                if (count == 0) {
                    clearInterval(counter);
                    players[index].control.vx = 5.5;
                }
                count--;
            }, 1000);
        }
        this.collide({ x: 0, y: 0, z: 1 }, callback);
    }

    update() {
        this.collideLeft();
        this.collideRight();
        this.collideUp();
        this.collideDown();
    }

}