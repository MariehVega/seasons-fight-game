class Bomb {
    constructor(element, x, y, z, ratio) {
        this.x = x;
        this.y = y;
        this.z = z;
        this.element = element;
        this.bombs = 0;
        this.count = 4;
        this.explosionRatio = ratio || 2;
    }

    set element(mesh) {
        if (mesh instanceof THREE.Mesh) {
            this._element = mesh;
        } else {
            let geometry = new THREE.SphereGeometry(50, 20, 20)
            let material = new THREE.MeshPhongMaterial({ color: 0x000000, wireframe: false });
            this._element = new THREE.Mesh(geometry, material);
            this._element.castShadow = true;
            this._element.receiveShadow = true;
            this._element.position.set(this.x, this.y, this.z)
        }
    }

    get element() {
        return this._element;
    }

    play(scene, control) {
        this.bombs = control.bombs;
        if (this.bombs == 1) {
            scene.add(this.element);
            collidableList.push(this.element);
            this.counter = setInterval(() => {
                timer(this, control);
            }, 1000);
        }
    }

    explosion() {
        let geometry = new THREE.BoxGeometry(125, 100, 125); //62 de radio
        var texturaExplosion = new THREE.MeshPhongMaterial({ map: THREE.ImageUtils.loadTexture('assets/textures/TexturaFuego.png') });
        var fireList = [];
        var cantidad = 4 * (this.explosionRatio);

        for (let i = 0; i < cantidad; i++) {
            fireList[i] = new THREE.Mesh(geometry, texturaExplosion);
        }

        fireList.push(new THREE.Mesh(geometry, texturaExplosion)); //Para el bloque de la mitad
        fireList[fireList.length - 1].position.set(this.x, this.y, this.z);

        var distancia = 125;

        for (let i = 1, k = 4, l = 0; i < (this.explosionRatio + 1); i++ , k += 4) {

            for (let j = l; j < k; j += 4) {

                if (k < fireList.length) {
                    fireList[j].position.set(this.x + (i * distancia), this.y, this.z);
                    fireList[j + 1].position.set(this.x - (i * distancia), this.y, this.z);
                    fireList[j + 2].position.set(this.x, this.y, this.z + (i * distancia));
                    fireList[j + 3].position.set(this.x, this.y, this.z - (i * distancia));
                }

            }

            l = k;
        }

        for (let i = 0; i < fireList.length; i++) {
            scene.add(fireList[i]);
            var soundExplosion = new Sound(["./assets/audio/Explosion.mp3"]);
            soundExplosion.volume = 0.5;
            soundExplosion.play();
            destroyableBox[i] = new DestroyableBox(fireList[i], 62);
        }

        this.counter = setInterval(() => {
            for (let i = 0; i < fireList.length; i++) {
                scene.remove(fireList[i]);
                destroyableBox.pop();
            }
        }, 1000);
    }

}

function timer(bomb, control) {
    if (bomb.count == 0) {
        clearInterval(bomb.counter);
        control.bombs = 0;
        scene.remove(bomb.element);
        var indexColli = collidableList.indexOf(bomb.element);
        collidableList.splice(indexColli, 1);
        bomb.explosion();
    }
    bomb.count--;
}