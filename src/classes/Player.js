class Player {

    constructor(name, element, control, character) {
        this.name = name;
        this.control = control;
        this.element = element;
    }

    set element(mesh) {
        if (mesh instanceof THREE.Mesh) {
            this._element = mesh;
            var helper = new THREE.BoundingBoxHelper(this._element, 0xffffff);
            helper.update();
            this._element.add(helper);

        } else {
            let geometry = new THREE.SphereGeometry(60, 20, 20);
            let material = new THREE.MeshPhongMaterial({
                color: Utilities.randomHexColor(),
                shininess: 2,
                specular: 0xffffff,
                shading: THREE.FlatShading
            });
            this._element = new THREE.Mesh(geometry, material);
            var helper = new THREE.BoundingBoxHelper(this._element, 0xffffff);
            helper.update();
            this._element.add(helper);
        }
        this.control.element = this._element;
    }

    get element() {
        return this._element;
    }

    updateControls() {
        this.control.update();
    }

    play(scene) {
        this.collidableBox = new CollidableBox(this._element, 60);
        this.element.position.x = 0;
        this.element.position.z = 0;
        destruibleList.push(this.element);
        scene.add(this.element);
    }
}