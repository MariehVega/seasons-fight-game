class CollidableBox {
    constructor(mesh, boundingRadius) {
        this.mesh = mesh;
        this.collidableRadius = boundingRadius;
    }

    collide(normal, callback) {
        let collidableRay = new THREE.Raycaster();
        collidableRay.ray.direction.set(normal.x, normal.y, normal.z);

        let origin = this.mesh.position.clone();
        collidableRay.ray.origin.copy(origin);

        let intersections = collidableRay.intersectObjects(collidableList);

        if (intersections.length > 0) {
            let distance = intersections[0].distance;
            if (distance <= this.collidableRadius-0.5) {
                callback();
            }
        }
    }

    collideLeft(controls) {
        let callback = () => {
            this.mesh.position.x += controls.vx;
        }
        this.collide({ x: -1, y: 0, z: 0 }, callback);
    }

    collideRight(controls) {
        let callback = () => {
            this.mesh.position.x -= controls.vx;
        }
        this.collide({ x: 1, y: 0, z: 0 }, callback);
    }

    collideUp(controls) {
        let callback = () => {
            this.mesh.position.z += controls.vx;
        }
        this.collide({ x: 0, y: 0, z: -1 }, callback);
    }

    collideDown(controls) {
        let callback = () => {
            this.mesh.position.z -= controls.vx;
        }
        this.collide({ x: 0, y: 0, z: 1 }, callback);
    }

    collideTop(controls){
        
        let callback = () => {
            controls.groundPosition = this.mesh.position.y;
        }
        if (controls.groundPosition>0) {
            controls.groundPosition -= controls.fg;
        }else if (controls.groundPosition==0){
            controls.groundPosition = 0;
        }

        this.collide({ x: 0, y: -1, z: 0 }, callback);
    }


    update(controls) {
        this.collideLeft(controls);
        this.collideRight(controls);
        this.collideUp(controls);
        this.collideDown(controls);
        this.collideTop(controls);
    }
}