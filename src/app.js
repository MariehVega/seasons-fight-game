cameras = {
    default: null,
    current: null,
    personal: null,
    invierno: null
};
canvas = {
    element: null,
    container: null
}

players = [];
playersMesh = [];
PUVelocityList = [];
PUPotencyList = [];
PUImmunityList = [];
destroyableBox = [];
destruibleList = [];
collidableList = [];
labels = {}
cameraControl = null;
scene = null;
renderer = null

// Lo que se muestra en pantalla 
let webGLStart = () => {
    initScene();
    window.onresize = onWindowResize;
    lastTime = Date.now();
    animateScene();
    tiempoLimite()
};

/**
 * Here we can setup all our scene noobsters
 */
function initScene() {
    //Selecting DOM Elements, the canvas and the parent element.
    canvas.container = document.querySelector("#app");
    canvas.element = canvas.container.querySelector("#appCanvas");

    /**
     * SETTING UP CORE THREEJS APP ELEMENTS (Scene, Cameras, Renderer)
     * */
    scene = new THREE.Scene();

    renderer = new THREE.WebGLRenderer({ canvas: canvas.element });
    renderer.setSize(canvas.container.clientWidth, canvas.container.clientHeight);
    renderer.setClearColor(0x000000, 1);

    canvas.container.appendChild(renderer.domElement);

    BackgroundTextura = new THREE.TextureLoader().load('assets/images/fondoBg.png',
        function (texture) {
            var img = texture.image;
            bgWidth = img.width;
            bgHeigth = img.heigth;
        }
    );

    scene.background = BackgroundTextura;

    // ---------------------- CAMARAS -----------------------------

    cameras.default = new THREE.PerspectiveCamera(45, canvas.container.clientWidth / canvas.container.clientHeight, 0.1, 10000);
    cameras.default.position.set(1, 1800, -91);
    cameras.default.lookAt(new THREE.Vector3(0, 0, 0));
    cameras.default.name = "Default";

    cameras.personal = new THREE.PerspectiveCamera(45, canvas.container.clientWidth / canvas.container.clientHeight, 1, 10000);
    cameras.personal.position.set(1600, 1600, -1600);
    cameras.personal.lookAt(new THREE.Vector3(0, 0, 0));
    cameras.personal.name = "Personal";

    cameras.invierno = new THREE.PerspectiveCamera(45, canvas.container.clientWidth / canvas.container.clientHeight, 0.1, 10000);
    cameras.invierno.position.set(2600, 400, -2600);
    cameras.invierno.lookAt(new THREE.Vector3(0, -3000, 0));
   
   // vector.applyQuaternion(cameras.invierno.quaternion);
    cameras.invierno.name = "Invierno";

    //Cargar jugadores
    for (let i = 0; i < players.length; i++) {
        players[i].play(scene);
        playersMesh[i] = players[i].element;
    }

    //Setting up current default camera as current camera
    cameras.current = cameras.default;

    //Camera control Plugin
    cameraControl = new THREE.OrbitControls(cameras.current, renderer.domElement);

    /*
    //FPS monitor
    stats = new Stats();
    stats.domElement.style.position = 'absolute';
    stats.domElement.style.Top = '10px';
    stats.domElement.style.left = '10px';
    stats.domElement.style.zIndex = '100';
    document.body.appendChild(stats.domElement);*/

    // Luz ambiente
    lAmbiente = new THREE.AmbientLight(0xffffff);
    scene.add(lAmbiente);

    initObjects();

}

function parar(){
    clearInterval(Contador);
}
function tiempoLimite(){
    count_Seconds=0;
    count_Minutes=0;

    seconds= document.getElementById("segundos");
    minutes=document.getElementById("minutes");
    Contador=setInterval(
        function(){
            if(count_Seconds==60){
                count_Seconds=0;
                count_Minutes++;
                minutes.innerHTML=count_Minutes;
                
                if(count_Minutes==3){
                    count_Minutes=0;
                }
                
            }
            seconds.innerHTML=count_Seconds;
                count_Seconds++;
        }
    ,1000)

}

function crearPersonajes(theCharacters) {
    let cuerpo = new THREE.SphereGeometry(60, 20, 20);
    let characters = {
        Michael: new THREE.MeshPhongMaterial({
            color: 0x0DA3EE,
            shininess: 2,
            specular: 0xffffff,
            shading: THREE.FlatShading
        }),
        Amber: new THREE.MeshPhongMaterial({
            color: 0xF467E9,
            shininess: 2,
            specular: 0xffffff,
            shading: THREE.FlatShading
        }),
        Jack: new THREE.MeshPhongMaterial({
            color: 0xF00808,
            shininess: 2,
            specular: 0xffffff,
            shading: THREE.FlatShading
        }),
        Natasha: new THREE.MeshPhongMaterial({
            color: 0xFEE240,
            shininess: 2,
            specular: 0xffffff,
            shading: THREE.FlatShading
        })
    }

    defaultControls = [
        new Control(),
        new Control("t", "h", "g", "f", "b", "v"),
        new Control("i", "l", "k", "j", "m", "n"),
        new Control("38", "39", "40", "37", " ", "13")
    ]

    for (let i = 0; i < maxIndex; i++) {
        var player = new THREE.Mesh(cuerpo.clone(), characters[theCharacters[i]].clone());
        players[i] = new Player(`P${i + 1}`, player, defaultControls[i]);
    }
}

function initObjects() {
    // Audio fondo

    Bgsound = new Sound(["./assets/audio/musicTheme.mp3"]);
    Bgsound.play();


    // ------------ PLANOS BORDES ----------------------
    var materialTransparente = new THREE.MeshPhongMaterial({ opacity: 0.0, transparent: true });

    // Primavera 
    geometriaPlanoPrimavera = new THREE.PlaneGeometry(1000, 200);

    var PrimaveraPlano1 = new THREE.Mesh(geometriaPlanoPrimavera, materialTransparente);
    PrimaveraPlano1.position.z -= 500;
    PrimaveraPlano1.position.y = 80;

    var PrimaveraPlano2 = new THREE.Mesh(geometriaPlanoPrimavera, materialTransparente);
    PrimaveraPlano2.position.x -= 500;
    PrimaveraPlano2.position.y = 80;
    PrimaveraPlano2.rotation.y = 1.5707696327;

    var PrimaveraPlano3 = new THREE.Mesh(geometriaPlanoPrimavera, materialTransparente);
    PrimaveraPlano3.position.z = 500;
    PrimaveraPlano3.position.y = 80;
    PrimaveraPlano3.rotation.y = 3.14159;

    var PrimaveraPlano4 = new THREE.Mesh(geometriaPlanoPrimavera, materialTransparente);
    PrimaveraPlano4.position.x = 500;
    PrimaveraPlano4.position.y = 80;
    PrimaveraPlano4.rotation.y = 1.5707696327;
    PrimaveraPlano4.rotation.y = 4.71239;

    // Otoño 
    geometriaPlanoOto = new THREE.PlaneGeometry(2000, 400);

    var OtoPlano1 = new THREE.Mesh(geometriaPlanoOto, materialTransparente);
    OtoPlano1.position.x = 1000;
    OtoPlano1.position.y = -1100;
    OtoPlano1.rotation.y = 1.5707696327;
    OtoPlano1.rotation.y = 4.71239;

    var OtoPlano2 = new THREE.Mesh(geometriaPlanoOto, materialTransparente);
    OtoPlano2.position.z = 1000;
    OtoPlano2.position.y = -1100;
    OtoPlano2.rotation.y = 3.14159;

    var OtoPlano3 = new THREE.Mesh(geometriaPlanoOto, materialTransparente);
    OtoPlano3.position.z = -1000;
    OtoPlano3.position.y = -1100;



    var OtoPlano4 = new THREE.Mesh(geometriaPlanoOto, materialTransparente);
    OtoPlano4.position.x = -1000;
    OtoPlano4.position.y = -1100;
    OtoPlano4.rotation.y = 1.5707696327;


    // Invierno 
    geometriaPlanoInv = new THREE.PlaneGeometry(3000, 400);
    var InvPlano1 = new THREE.Mesh(geometriaPlanoInv, materialTransparente);
    InvPlano1.position.x = 1500;
    InvPlano1.position.y = -2350;
    InvPlano1.rotation.y = 1.5707696327;
    InvPlano1.rotation.y = 4.71239;

    var InvPlano2 = new THREE.Mesh(geometriaPlanoInv, materialTransparente);
    InvPlano2.position.x = -1500;
    InvPlano2.position.y = -2350;
    InvPlano2.rotation.y = 1.5707696327;

    var InvPlano3 = new THREE.Mesh(geometriaPlanoInv, materialTransparente);
    InvPlano3.position.z = -1500;
    InvPlano3.position.y = -2350;

    var InvPlano4 = new THREE.Mesh(geometriaPlanoInv, materialTransparente);
    InvPlano4.position.z = 1500;
    InvPlano4.position.y = -2350;
    InvPlano4.rotation.y = 3.14159;

    scene.add(PrimaveraPlano1, PrimaveraPlano2, PrimaveraPlano3, PrimaveraPlano4, OtoPlano1, OtoPlano2, OtoPlano3,
        OtoPlano4, InvPlano1, InvPlano4, InvPlano2, InvPlano3);
    //---------------------------------------------------------------------------------------------------------------------------------

    var SueloPrimavera = new THREE.BoxGeometry(1000, 56.66, 1000);
    var BloquesPrimavera = new THREE.BoxGeometry(125, 100, 125);

    var SueloPInvierno = new THREE.BoxGeometry(3000, 170, 3000);
    var BloquesInvierno = new THREE.BoxGeometry(167, 160, 167);
    var BloquesHielo = new THREE.BoxGeometry(167, 160, 167);

    var SueloOtonio = new THREE.BoxGeometry(2000, 100, 2000);
    var BloquesOtonio = new THREE.BoxGeometry(154, 140, 154);

    var TronoOtonio = new THREE.BoxGeometry(154, 140, 154);
    var TronoInvierno = new THREE.BoxGeometry(167, 160, 167);
    var TronoPrimavera = new THREE.BoxGeometry(125, 100, 125);

    var PlataformaPrimavera = new THREE.BoxGeometry(125, 15, 125);
    var PlataformaInvierno = new THREE.BoxGeometry(167, 35, 167);
    var PlataformaOtonio = new THREE.BoxGeometry(154, 25, 154);
    var PlataformaOtonioSP = new THREE.BoxGeometry(135, 20, 135);

    // Power ups
    var PUInv = new THREE.BoxGeometry(100, 100, 100);

    //UV Textura
    var texturaPrimSuelo = new THREE.MeshPhongMaterial({ map: THREE.ImageUtils.loadTexture('assets/textures/TexturaPisoPrim.png') })
    var texturaInvSuelo = new THREE.MeshPhongMaterial({ map: THREE.ImageUtils.loadTexture('assets/textures/TexturaPisoInvierno.png') })
    var texturaOtSuelo = new THREE.MeshPhongMaterial({ map: THREE.ImageUtils.loadTexture('assets/textures/TexturaPisoOtoño.png') })
    var texturaPasto = new THREE.MeshPhongMaterial({ map: THREE.ImageUtils.loadTexture('assets/textures/TexturaPasto.png') })
    var texturaPiedraPrim = new THREE.MeshPhongMaterial({ map: THREE.ImageUtils.loadTexture('assets/textures/TexturaPiedraVerano.png') })
    var texturaPiedraInv = new THREE.MeshPhongMaterial({ map: THREE.ImageUtils.loadTexture('assets/textures/TexturaPiedraInvierno.png') })
    var texturaPiedraOto = new THREE.MeshPhongMaterial({ map: THREE.ImageUtils.loadTexture('assets/textures/TexturaPiedraOtoño.png') })
    var texturaHojasOto = new THREE.MeshPhongMaterial({ map: THREE.ImageUtils.loadTexture('assets/textures/TexturaHojasOto.png') })
    var texturaTronoOtoVer = new THREE.MeshPhongMaterial({ map: THREE.ImageUtils.loadTexture('assets/textures/TronoOtoVerano.png') })
    var texturaTronoInv = new THREE.MeshPhongMaterial({ map: THREE.ImageUtils.loadTexture('assets/textures/TronoInv.png') })
    var texturPlataformaPrim = new THREE.MeshPhongMaterial({ map: THREE.ImageUtils.loadTexture('assets/textures/PlataformaPrimavera.png') })
    var texturPlataformaInv = new THREE.MeshPhongMaterial({ map: THREE.ImageUtils.loadTexture('assets/textures/PlataformaInvierno.png') })
    var texturPlataformaOto1 = new THREE.MeshPhongMaterial({ map: THREE.ImageUtils.loadTexture('assets/textures/PlataformaOtonio1.png') })
    var texturPlataformaOto2 = new THREE.MeshPhongMaterial({ map: THREE.ImageUtils.loadTexture('assets/textures/PlataformaOtonio2.png') })
    var texturaHielo = new THREE.MeshPhongMaterial({ map: THREE.ImageUtils.loadTexture('assets/textures/texturaHielo.png') })

    var texturaEstrella = new THREE.MeshPhongMaterial({ map: THREE.ImageUtils.loadTexture('assets/textures/Estrella.png'), transparent: true })
    var texturaVelocidadInvierno = new THREE.MeshPhongMaterial({ map: THREE.ImageUtils.loadTexture('assets/textures/TexturasVelocidadInv.png'), transparent: true })
    var texturaVelocidadOtonio = new THREE.MeshPhongMaterial({ map: THREE.ImageUtils.loadTexture('assets/textures/TexturasVelocidadOto.png'), transparent: true })
    var texturaVelocidadPrimavera = new THREE.MeshPhongMaterial({ map: THREE.ImageUtils.loadTexture('assets/textures/TexturasVelocidadPrim.png'), transparent: true })
    var texturaBombasInv = new THREE.MeshPhongMaterial({ map: THREE.ImageUtils.loadTexture('assets/textures/TexturasBombax2Inv.png'), transparent: true })
    var texturaBombas = new THREE.MeshPhongMaterial({ map: THREE.ImageUtils.loadTexture('assets/textures/TexturasBombax2PrimOto.png'), transparent: true })
    var texturaVida = new THREE.MeshPhongMaterial({ map: THREE.ImageUtils.loadTexture('assets/textures/TexturasVida.png'), transparent: true })


    var UV = {
        SuelosBase: [
            new THREE.Vector2(0.3, 0.8),
            new THREE.Vector2(0.3, 0.7),
            new THREE.Vector2(0.8, 0.7),
            new THREE.Vector2(0.8, 0.8)
        ],

        SuelosSombraClara: [
            new THREE.Vector2(0.0, 0.60),
            new THREE.Vector2(0.0, 0.40),
            new THREE.Vector2(0.8, 0.40),
            new THREE.Vector2(0.8, 0.60)
        ],

        SueloSombraOscuro: [
            new THREE.Vector2(0.0, 0.30),
            new THREE.Vector2(0.0, 0.1),
            new THREE.Vector2(0.8, 0.1),
            new THREE.Vector2(0.8, 0.30)
        ],

        Plataforma: [
            new THREE.Vector2(0.0, 1),
            new THREE.Vector2(0.0, 0.33),
            new THREE.Vector2(1, 0.33),
            new THREE.Vector2(1, 1)
        ]
    }

    //-----------------caras-----------------

    SueloPrimavera.faceVertexUvs[0] = [];
    SueloPInvierno.faceVertexUvs[0] = [];
    SueloOtonio.faceVertexUvs[0] = [];
    TronoOtonio.faceVertexUvs[0] = [];
    TronoInvierno.faceVertexUvs[0] = [];
    TronoPrimavera.faceVertexUvs[0] = [];
    PlataformaPrimavera.faceVertexUvs[0] = [];
    PlataformaInvierno.faceVertexUvs[0] = [];
    PlataformaOtonio.faceVertexUvs[0] = [];
    PlataformaOtonioSP.faceVertexUvs[0] = [];
    BloquesHielo.faceVertexUvs[0] = [];

    //----------------------Primavera-----------------
    // ------------Suelo------------------------------
    SueloPrimavera.faceVertexUvs[0][4] = [UV.SuelosBase[0], UV.SuelosBase[1], UV.SuelosBase[3]];
    SueloPrimavera.faceVertexUvs[0][5] = [UV.SuelosBase[1], UV.SuelosBase[2], UV.SuelosBase[3]];

    // SombrasAbajo-Claras
    SueloPrimavera.faceVertexUvs[0][8] = [UV.SuelosSombraClara[0], UV.SuelosSombraClara[1], UV.SuelosSombraClara[3]];
    SueloPrimavera.faceVertexUvs[0][9] = [UV.SuelosSombraClara[1], UV.SuelosSombraClara[2], UV.SuelosSombraClara[3]];
    SueloPrimavera.faceVertexUvs[0][2] = [UV.SuelosSombraClara[0], UV.SuelosSombraClara[1], UV.SuelosSombraClara[3]];
    SueloPrimavera.faceVertexUvs[0][3] = [UV.SuelosSombraClara[1], UV.SuelosSombraClara[2], UV.SuelosSombraClara[3]];

    // SombrasAbajo-Oscuras
    SueloPrimavera.faceVertexUvs[0][10] = [UV.SueloSombraOscuro[0], UV.SueloSombraOscuro[1], UV.SueloSombraOscuro[3]];
    SueloPrimavera.faceVertexUvs[0][11] = [UV.SueloSombraOscuro[1], UV.SueloSombraOscuro[2], UV.SueloSombraOscuro[3]];
    SueloPrimavera.faceVertexUvs[0][0] = [UV.SueloSombraOscuro[0], UV.SueloSombraOscuro[1], UV.SueloSombraOscuro[3]];
    SueloPrimavera.faceVertexUvs[0][1] = [UV.SueloSombraOscuro[1], UV.SueloSombraOscuro[2], UV.SueloSombraOscuro[3]];

    // -----------Plataformas---------------------------------
    PlataformaPrimavera.faceVertexUvs[0][4] = [UV.Plataforma[0], UV.Plataforma[1], UV.Plataforma[3]];
    PlataformaPrimavera.faceVertexUvs[0][5] = [UV.Plataforma[1], UV.Plataforma[2], UV.Plataforma[3]];

    //----------------------Invierno-----------------
    // ------------Suelo------------------------------
    SueloPInvierno.faceVertexUvs[0][4] = [UV.SuelosBase[0], UV.SuelosBase[1], UV.SuelosBase[3]];
    SueloPInvierno.faceVertexUvs[0][5] = [UV.SuelosBase[1], UV.SuelosBase[2], UV.SuelosBase[3]];

    // SombrasAbajo-Claras
    SueloPInvierno.faceVertexUvs[0][8] = [UV.SuelosSombraClara[0], UV.SuelosSombraClara[1], UV.SuelosSombraClara[3]];
    SueloPInvierno.faceVertexUvs[0][9] = [UV.SuelosSombraClara[1], UV.SuelosSombraClara[2], UV.SuelosSombraClara[3]];
    SueloPInvierno.faceVertexUvs[0][2] = [UV.SuelosSombraClara[0], UV.SuelosSombraClara[1], UV.SuelosSombraClara[3]];
    SueloPInvierno.faceVertexUvs[0][3] = [UV.SuelosSombraClara[1], UV.SuelosSombraClara[2], UV.SuelosSombraClara[3]];

    // SombrasAbajo-Oscuras
    SueloPInvierno.faceVertexUvs[0][10] = [UV.SueloSombraOscuro[0], UV.SueloSombraOscuro[1], UV.SueloSombraOscuro[3]];
    SueloPInvierno.faceVertexUvs[0][11] = [UV.SueloSombraOscuro[1], UV.SueloSombraOscuro[2], UV.SueloSombraOscuro[3]];
    SueloPInvierno.faceVertexUvs[0][0] = [UV.SueloSombraOscuro[0], UV.SueloSombraOscuro[1], UV.SueloSombraOscuro[3]];
    SueloPInvierno.faceVertexUvs[0][1] = [UV.SueloSombraOscuro[1], UV.SueloSombraOscuro[2], UV.SueloSombraOscuro[3]];

    // -----------Plataformas---------------------------------
    PlataformaInvierno.faceVertexUvs[0][4] = [UV.Plataforma[0], UV.Plataforma[1], UV.Plataforma[3]];
    PlataformaInvierno.faceVertexUvs[0][5] = [UV.Plataforma[1], UV.Plataforma[2], UV.Plataforma[3]];

    // ------------ Bloques Hielo 
    // ------------Arriba------------------------------
    /*BloquesHielo.faceVertexUvs[0][4] = [UV.Plataforma[0], UV.SuelosBase[1], UV.SuelosBase[3]];
    BloquesHielo.faceVertexUvs[0][5] = [UV.Plataforma[1], UV.SuelosBase[2], UV.SuelosBase[3]];*/

    // Lados
    BloquesHielo.faceVertexUvs[0][8] = [UV.Plataforma[0], UV.Plataforma[1], UV.Plataforma[3]];
    BloquesHielo.faceVertexUvs[0][9] = [UV.Plataforma[1], UV.Plataforma[2], UV.Plataforma[3]];
    BloquesHielo.faceVertexUvs[0][2] = [UV.Plataforma[0], UV.Plataforma[1], UV.Plataforma[3]];
    BloquesHielo.faceVertexUvs[0][3] = [UV.Plataforma[1], UV.Plataforma[2], UV.Plataforma[3]];

    BloquesHielo.faceVertexUvs[0][10] = [UV.Plataforma[0], UV.Plataforma[1], UV.Plataforma[3]];
    BloquesHielo.faceVertexUvs[0][11] = [UV.Plataforma[1], UV.Plataforma[2], UV.Plataforma[3]];
    BloquesHielo.faceVertexUvs[0][0] = [UV.Plataforma[0], UV.Plataforma[1], UV.Plataforma[3]];
    BloquesHielo.faceVertexUvs[0][1] = [UV.Plataforma[1], UV.Plataforma[2], UV.Plataforma[3]];



    //----------------------Otoño-----------------
    // ------------Suelo------------------------------
    SueloOtonio.faceVertexUvs[0][4] = [UV.SuelosBase[0], UV.SuelosBase[1], UV.SuelosBase[3]];
    SueloOtonio.faceVertexUvs[0][5] = [UV.SuelosBase[1], UV.SuelosBase[2], UV.SuelosBase[3]];

    // SombrasAbajo-Claras
    SueloOtonio.faceVertexUvs[0][8] = [UV.SuelosSombraClara[0], UV.SuelosSombraClara[1], UV.SuelosSombraClara[3]];
    SueloOtonio.faceVertexUvs[0][9] = [UV.SuelosSombraClara[1], UV.SuelosSombraClara[2], UV.SuelosSombraClara[3]];
    SueloOtonio.faceVertexUvs[0][2] = [UV.SuelosSombraClara[0], UV.SuelosSombraClara[1], UV.SuelosSombraClara[3]];
    SueloOtonio.faceVertexUvs[0][3] = [UV.SuelosSombraClara[1], UV.SuelosSombraClara[2], UV.SuelosSombraClara[3]];

    // SombrasAbajo-Oscuras
    SueloOtonio.faceVertexUvs[0][10] = [UV.SueloSombraOscuro[0], UV.SueloSombraOscuro[1], UV.SueloSombraOscuro[3]];
    SueloOtonio.faceVertexUvs[0][11] = [UV.SueloSombraOscuro[1], UV.SueloSombraOscuro[2], UV.SueloSombraOscuro[3]];
    SueloOtonio.faceVertexUvs[0][0] = [UV.SueloSombraOscuro[0], UV.SueloSombraOscuro[1], UV.SueloSombraOscuro[3]];
    SueloOtonio.faceVertexUvs[0][1] = [UV.SueloSombraOscuro[1], UV.SueloSombraOscuro[2], UV.SueloSombraOscuro[3]];

    // -----------Plataformas---------------------------------
    PlataformaOtonio.faceVertexUvs[0][4] = [UV.Plataforma[0], UV.Plataforma[1], UV.Plataforma[3]];
    PlataformaOtonio.faceVertexUvs[0][5] = [UV.Plataforma[1], UV.Plataforma[2], UV.Plataforma[3]];

    PlataformaOtonioSP.faceVertexUvs[0][4] = [UV.Plataforma[0], UV.Plataforma[1], UV.Plataforma[3]];
    PlataformaOtonioSP.faceVertexUvs[0][5] = [UV.Plataforma[1], UV.Plataforma[2], UV.Plataforma[3]];

    //----------------------Tronos-----------------
    // Otoño
    TronoOtonio.faceVertexUvs[0][4] = [UV.SuelosBase[0], UV.SuelosBase[1], UV.SuelosBase[3]];
    TronoOtonio.faceVertexUvs[0][5] = [UV.SuelosBase[1], UV.SuelosBase[2], UV.SuelosBase[3]];

    TronoOtonio.faceVertexUvs[0][8] = [UV.SuelosSombraClara[0], UV.SuelosSombraClara[1], UV.SuelosSombraClara[3]];
    TronoOtonio.faceVertexUvs[0][9] = [UV.SuelosSombraClara[1], UV.SuelosSombraClara[2], UV.SuelosSombraClara[3]];
    TronoOtonio.faceVertexUvs[0][2] = [UV.SuelosSombraClara[0], UV.SuelosSombraClara[1], UV.SuelosSombraClara[3]];
    TronoOtonio.faceVertexUvs[0][3] = [UV.SuelosSombraClara[1], UV.SuelosSombraClara[2], UV.SuelosSombraClara[3]];

    TronoOtonio.faceVertexUvs[0][10] = [UV.SuelosSombraClara[0], UV.SuelosSombraClara[1], UV.SuelosSombraClara[3]];
    TronoOtonio.faceVertexUvs[0][11] = [UV.SuelosSombraClara[1], UV.SuelosSombraClara[2], UV.SuelosSombraClara[3]];
    TronoOtonio.faceVertexUvs[0][0] = [UV.SuelosSombraClara[0], UV.SuelosSombraClara[1], UV.SuelosSombraClara[3]];
    TronoOtonio.faceVertexUvs[0][1] = [UV.SuelosSombraClara[1], UV.SuelosSombraClara[2], UV.SuelosSombraClara[3]];

    // Invierno
    TronoInvierno.faceVertexUvs[0][4] = [UV.SuelosBase[0], UV.SuelosBase[1], UV.SuelosBase[3]];
    TronoInvierno.faceVertexUvs[0][5] = [UV.SuelosBase[1], UV.SuelosBase[2], UV.SuelosBase[3]];

    TronoInvierno.faceVertexUvs[0][8] = [UV.SuelosSombraClara[0], UV.SuelosSombraClara[1], UV.SuelosSombraClara[3]];
    TronoInvierno.faceVertexUvs[0][9] = [UV.SuelosSombraClara[1], UV.SuelosSombraClara[2], UV.SuelosSombraClara[3]];
    TronoInvierno.faceVertexUvs[0][2] = [UV.SuelosSombraClara[0], UV.SuelosSombraClara[1], UV.SuelosSombraClara[3]];
    TronoInvierno.faceVertexUvs[0][3] = [UV.SuelosSombraClara[1], UV.SuelosSombraClara[2], UV.SuelosSombraClara[3]];

    TronoInvierno.faceVertexUvs[0][10] = [UV.SuelosSombraClara[0], UV.SuelosSombraClara[1], UV.SuelosSombraClara[3]];
    TronoInvierno.faceVertexUvs[0][11] = [UV.SuelosSombraClara[1], UV.SuelosSombraClara[2], UV.SuelosSombraClara[3]];
    TronoInvierno.faceVertexUvs[0][0] = [UV.SuelosSombraClara[0], UV.SuelosSombraClara[1], UV.SuelosSombraClara[3]];
    TronoInvierno.faceVertexUvs[0][1] = [UV.SuelosSombraClara[1], UV.SuelosSombraClara[2], UV.SuelosSombraClara[3]];

    // Primavera
    TronoPrimavera.faceVertexUvs[0][4] = [UV.SuelosBase[0], UV.SuelosBase[1], UV.SuelosBase[3]];
    TronoPrimavera.faceVertexUvs[0][5] = [UV.SuelosBase[1], UV.SuelosBase[2], UV.SuelosBase[3]];

    TronoPrimavera.faceVertexUvs[0][8] = [UV.SuelosSombraClara[0], UV.SuelosSombraClara[1], UV.SuelosSombraClara[3]];
    TronoPrimavera.faceVertexUvs[0][9] = [UV.SuelosSombraClara[1], UV.SuelosSombraClara[2], UV.SuelosSombraClara[3]];
    TronoPrimavera.faceVertexUvs[0][2] = [UV.SuelosSombraClara[0], UV.SuelosSombraClara[1], UV.SuelosSombraClara[3]];
    TronoPrimavera.faceVertexUvs[0][3] = [UV.SuelosSombraClara[1], UV.SuelosSombraClara[2], UV.SuelosSombraClara[3]];

    TronoPrimavera.faceVertexUvs[0][10] = [UV.SuelosSombraClara[0], UV.SuelosSombraClara[1], UV.SuelosSombraClara[3]];
    TronoPrimavera.faceVertexUvs[0][11] = [UV.SuelosSombraClara[1], UV.SuelosSombraClara[2], UV.SuelosSombraClara[3]];
    TronoPrimavera.faceVertexUvs[0][0] = [UV.SuelosSombraClara[0], UV.SuelosSombraClara[1], UV.SuelosSombraClara[3]];
    TronoPrimavera.faceVertexUvs[0][1] = [UV.SuelosSombraClara[1], UV.SuelosSombraClara[2], UV.SuelosSombraClara[3]];

    //----------------FILTRO
    texturaPasto.map.wrapS = texturaPasto.map.wrapT = THREE.MirroredRepeatWrapping;
    texturaPasto.map.repeat.set(2, 2);

    /*------------------------------------------------------ PRIMAVERA -------------------------------------------------*/

    PrimaveraB = new THREE.Mesh(SueloPrimavera, texturaPrimSuelo);
    BloquesPrim1 = new THREE.Mesh(BloquesPrimavera, texturaPiedraPrim);
    BloquesPrim2 = new THREE.Mesh(BloquesPrimavera, texturaPasto);
    BloquesPrim3 = new THREE.Mesh(BloquesPrimavera, texturaPasto);
    BloquesPrim4 = new THREE.Mesh(BloquesPrimavera, texturaPiedraPrim);
    BloquesPrim5 = new THREE.Mesh(BloquesPrimavera, texturaPiedraPrim);
    BloquesPrim6 = new THREE.Mesh(BloquesPrimavera, texturaPasto);
    BloquesPrim7 = new THREE.Mesh(BloquesPrimavera, texturaPiedraPrim);
    BloquesPrim8 = new THREE.Mesh(BloquesPrimavera, texturaPasto);
    BloquesPrim9 = new THREE.Mesh(BloquesPrimavera, texturaPasto);
    BloquesPrim10 = new THREE.Mesh(BloquesPrimavera, texturaPiedraPrim);
    BloquesPrim11 = new THREE.Mesh(BloquesPrimavera, texturaPasto);
    BloquesPrim12 = new THREE.Mesh(BloquesPrimavera, texturaPasto);
    BloquesPrim13 = new THREE.Mesh(BloquesPrimavera, texturaPiedraPrim);
    BloquesPrim14 = new THREE.Mesh(BloquesPrimavera, texturaPasto);
    BloquesPrim15 = new THREE.Mesh(BloquesPrimavera, texturaPiedraPrim);
    BloquesPrim16 = new THREE.Mesh(BloquesPrimavera, texturaPiedraPrim);
    BloquesPrim17 = new THREE.Mesh(BloquesPrimavera, texturaPasto);
    BloquesPrim18 = new THREE.Mesh(BloquesPrimavera, texturaPiedraPrim);
    BloquesPrim19 = new THREE.Mesh(BloquesPrimavera, texturaPiedraPrim);
    BloquesPrim20 = new THREE.Mesh(BloquesPrimavera, texturaPasto);
    BloquesPrim21 = new THREE.Mesh(BloquesPrimavera, texturaPiedraPrim);
    BloquesPrim22 = new THREE.Mesh(BloquesPrimavera, texturaPasto);
    BloquesPrim23 = new THREE.Mesh(BloquesPrimavera, texturaPasto);

    BloquesPrimEstrella1 = new THREE.Mesh(TronoPrimavera, texturaTronoOtoVer);
    BloquesPrimEstrella2 = new THREE.Mesh(TronoPrimavera, texturaTronoOtoVer);
    BloquesPrimEstrella3 = new THREE.Mesh(TronoPrimavera, texturaTronoOtoVer);
    BloquesPrimEstrella4 = new THREE.Mesh(TronoPrimavera, texturaTronoOtoVer);
    BloquesPrimEstrella5 = new THREE.Mesh(TronoPrimavera, texturaTronoOtoVer);

    Elevador = new THREE.Mesh(PlataformaPrimavera, texturPlataformaPrim);

    //POSICIONES 
    // ---Linea 1---
    BloquesPrim1.position.y = 78.33;
    BloquesPrim1.position.x -= 437.66;
    BloquesPrim1.position.z = 437.66;

    BloquesPrim2.position.y = 78.33;
    BloquesPrim2.position.x -= 312.66;
    BloquesPrim2.position.z = 437.66;

    BloquesPrim3.position.y = 78.33;
    BloquesPrim3.position.x -= 63;
    BloquesPrim3.position.z = 437.66;

    BloquesPrim4.position.y = 78.33;
    BloquesPrim4.position.x = 437.33;
    BloquesPrim4.position.z = 437.66;

    // ---Linea 2---
    BloquesPrim5.position.y = 78.33;
    BloquesPrim5.position.x -= 187.66;
    BloquesPrim5.position.z = 312.66;

    BloquesPrim6.position.y = 78.33;
    BloquesPrim6.position.x -= 63;
    BloquesPrim6.position.z = 312.66;

    BloquesPrim7.position.y = 78.33;
    BloquesPrim7.position.x = 62;
    BloquesPrim7.position.z = 312.66;

    BloquesPrim8.position.y = 78.33;
    BloquesPrim8.position.x = 187;
    BloquesPrim8.position.z = 312.66;

    // ---Linea 3---
    BloquesPrim9.position.y = 78.33;
    BloquesPrim9.position.x = 62;
    BloquesPrim9.position.z = 187;

    BloquesPrim10.position.y = 78.33;
    BloquesPrim10.position.x = 312;
    BloquesPrim10.position.z = 187;

    // ---Linea 4----
    BloquesPrim11.position.y = 78.33;
    BloquesPrim11.position.x -= 312.66;
    BloquesPrim11.position.z = 63;

    BloquesPrim12.position.y = 78.33;
    BloquesPrim12.position.x = 312.33;
    BloquesPrim12.position.z = 63;

    BloquesPrim13.position.y = 78.33;
    BloquesPrim13.position.x = 437.33;
    BloquesPrim13.position.z = 63;

    // ---Linea 5---
    BloquesPrim14.position.y = 78.33;
    BloquesPrim14.position.x -= 437.66;
    BloquesPrim14.position.z -= 62;

    BloquesPrim15.position.y = 78.33;
    BloquesPrim15.position.x -= 312.66;
    BloquesPrim15.position.z -= 62;

    // ---Linea 6---
    BloquesPrim16.position.y = 78.33;
    BloquesPrim16.position.x = 187.33;
    BloquesPrim16.position.z -= 187;

    // ---Linea 7---
    BloquesPrim17.position.y = 78.33;
    BloquesPrim17.position.x -= 312.66;
    BloquesPrim17.position.z -= 312;

    BloquesPrim18.position.y = 78.33;
    BloquesPrim18.position.x = 437.33;
    BloquesPrim18.position.z -= 312;

    // ---Linea 8---
    BloquesPrim19.position.y = 78.33;
    BloquesPrim19.position.x -= 437.66;
    BloquesPrim19.position.z -= 437;

    BloquesPrim20.position.y = 78.33;
    BloquesPrim20.position.x -= 187.66;
    BloquesPrim20.position.z -= 437;

    BloquesPrim21.position.y = 78.33;
    BloquesPrim21.position.x -= 63;
    BloquesPrim21.position.z -= 437;

    BloquesPrim22.position.y = 78.33;
    BloquesPrim22.position.x = 62;
    BloquesPrim22.position.z -= 437;

    BloquesPrim23.position.y = 78.33;
    BloquesPrim23.position.x = 312.33;
    BloquesPrim23.position.z -= 437;

    BloquesPrimEstrella1.position.y = 78.33;
    BloquesPrimEstrella1.position.x -= 62.66;
    BloquesPrimEstrella1.position.z -= 62;

    BloquesPrimEstrella2.position.y = 178.33;
    BloquesPrimEstrella2.position.x -= 62.66;
    BloquesPrimEstrella2.position.z -= 62;

    BloquesPrimEstrella3.position.y = 278.33;
    BloquesPrimEstrella3.position.x -= 62.66;
    BloquesPrimEstrella3.position.z -= 62;

    BloquesPrimEstrella4.position.y = 378.33;
    BloquesPrimEstrella4.position.x -= 62.66;
    BloquesPrimEstrella4.position.z -= 62;

    BloquesPrimEstrella5.position.y = 478.33;
    BloquesPrimEstrella5.position.x -= 62.66;
    BloquesPrimEstrella5.position.z -= 62;

    Elevador.position.y = 35;
    Elevador.position.x = 62.34;
    Elevador.position.z -= 62;

 /*  scene.add(PrimaveraB, BloquesPrim1, BloquesPrim2, BloquesPrim3, BloquesPrim4,
        BloquesPrim5, BloquesPrim6, BloquesPrim7, BloquesPrim8, BloquesPrim9,
        BloquesPrim10, BloquesPrim11, BloquesPrim12, BloquesPrim13, BloquesPrim14,
        BloquesPrim15, BloquesPrim16, BloquesPrim17, BloquesPrim18, BloquesPrim19,
        BloquesPrim20, BloquesPrim21, BloquesPrim22, BloquesPrim23, BloquesPrimEstrella1,
        BloquesPrimEstrella2, BloquesPrimEstrella3, BloquesPrimEstrella4, BloquesPrimEstrella5,
        Elevador);*/


    /*------------------------------------------------------ OTOÑO -------------------------------------------------*/

    OtonioB = new THREE.Mesh(SueloOtonio, texturaOtSuelo);
    BloquesOto1 = new THREE.Mesh(BloquesOtonio, texturaHojasOto);
    BloquesOto2 = new THREE.Mesh(BloquesOtonio, texturaHojasOto);
    BloquesOto3 = new THREE.Mesh(BloquesOtonio, texturaPiedraOto);
    BloquesOto4 = new THREE.Mesh(BloquesOtonio, texturaPiedraOto);
    BloquesOto5 = new THREE.Mesh(BloquesOtonio, texturaPiedraOto);
    BloquesOto6 = new THREE.Mesh(BloquesOtonio, texturaPiedraOto);
    BloquesOto7 = new THREE.Mesh(BloquesOtonio, texturaPiedraOto);
    BloquesOto8 = new THREE.Mesh(BloquesOtonio, texturaHojasOto);
    BloquesOto9 = new THREE.Mesh(BloquesOtonio, texturaHojasOto);
    BloquesOto10 = new THREE.Mesh(BloquesOtonio, texturaPiedraOto);
    BloquesOto11 = new THREE.Mesh(BloquesOtonio, texturaHojasOto);
    BloquesOto12 = new THREE.Mesh(BloquesOtonio, texturaPiedraOto);
    BloquesOto13 = new THREE.Mesh(BloquesOtonio, texturaHojasOto);
    BloquesOto14 = new THREE.Mesh(BloquesOtonio, texturaPiedraOto);
    BloquesOto15 = new THREE.Mesh(BloquesOtonio, texturaHojasOto);
    BloquesOto16 = new THREE.Mesh(BloquesOtonio, texturaHojasOto);
    BloquesOto17 = new THREE.Mesh(BloquesOtonio, texturaPiedraOto);
    BloquesOto18 = new THREE.Mesh(BloquesOtonio, texturaHojasOto);
    BloquesOto19 = new THREE.Mesh(BloquesOtonio, texturaHojasOto);
    BloquesOto20 = new THREE.Mesh(BloquesOtonio, texturaHojasOto);
    BloquesOto21 = new THREE.Mesh(BloquesOtonio, texturaHojasOto);
    BloquesOto22 = new THREE.Mesh(BloquesOtonio, texturaPiedraOto);
    BloquesOto23 = new THREE.Mesh(BloquesOtonio, texturaPiedraOto);
    BloquesOto24 = new THREE.Mesh(BloquesOtonio, texturaHojasOto);
    BloquesOto25 = new THREE.Mesh(BloquesOtonio, texturaPiedraOto);
    BloquesOto26 = new THREE.Mesh(BloquesOtonio, texturaHojasOto);
    BloquesOto27 = new THREE.Mesh(BloquesOtonio, texturaPiedraOto);
    BloquesOto28 = new THREE.Mesh(BloquesOtonio, texturaPiedraOto);
    BloquesOto29 = new THREE.Mesh(BloquesOtonio, texturaHojasOto);
    BloquesOto30 = new THREE.Mesh(BloquesOtonio, texturaHojasOto);
    BloquesOto31 = new THREE.Mesh(BloquesOtonio, texturaPiedraOto);
    BloquesOto32 = new THREE.Mesh(BloquesOtonio, texturaHojasOto);
    BloquesOto33 = new THREE.Mesh(BloquesOtonio, texturaHojasOto);
    BloquesOto34 = new THREE.Mesh(BloquesOtonio, texturaPiedraOto);
    BloquesOto35 = new THREE.Mesh(BloquesOtonio, texturaHojasOto);
    BloquesOto36 = new THREE.Mesh(BloquesOtonio, texturaPiedraOto);
    BloquesOto37 = new THREE.Mesh(BloquesOtonio, texturaPiedraOto);
    BloquesOto38 = new THREE.Mesh(BloquesOtonio, texturaHojasOto);
    BloquesOto39 = new THREE.Mesh(BloquesOtonio, texturaHojasOto);
    BloquesOto40 = new THREE.Mesh(BloquesOtonio, texturaPiedraOto);
    BloquesOto41 = new THREE.Mesh(BloquesOtonio, texturaHojasOto);
    BloquesOto42 = new THREE.Mesh(BloquesOtonio, texturaHojasOto);
    BloquesOto43 = new THREE.Mesh(BloquesOtonio, texturaPiedraOto);
    BloquesOto44 = new THREE.Mesh(BloquesOtonio, texturaPiedraOto);
    BloquesOto45 = new THREE.Mesh(BloquesOtonio, texturaHojasOto);
    BloquesOto46 = new THREE.Mesh(BloquesOtonio, texturaPiedraOto);
    BloquesOto47 = new THREE.Mesh(BloquesOtonio, texturaHojasOto);
    BloquesOto48 = new THREE.Mesh(BloquesOtonio, texturaPiedraOto);
    BloquesOto49 = new THREE.Mesh(BloquesOtonio, texturaHojasOto);
    BloquesOto50 = new THREE.Mesh(BloquesOtonio, texturaPiedraOto);
    BloquesOto51 = new THREE.Mesh(BloquesOtonio, texturaPiedraOto);
    BloquesOto52 = new THREE.Mesh(BloquesOtonio, texturaHojasOto);
    BloquesOto53 = new THREE.Mesh(BloquesOtonio, texturaPiedraOto);
    BloquesOto54 = new THREE.Mesh(BloquesOtonio, texturaHojasOto);
    BloquesOto55 = new THREE.Mesh(BloquesOtonio, texturaPiedraOto);
    BloquesOtoSP1 = new THREE.Mesh(BloquesOtonio, texturaPiedraOto);
    BloquesOtoSP2 = new THREE.Mesh(BloquesOtonio, texturaPiedraOto);
    BloquesOtoSP3 = new THREE.Mesh(BloquesOtonio, texturaPiedraOto);
    BloquesOtoSP4 = new THREE.Mesh(BloquesOtonio, texturaPiedraOto);
    BloquesOtoSP5 = new THREE.Mesh(BloquesOtonio, texturaPiedraOto);
    BloquesOtoSP6 = new THREE.Mesh(BloquesOtonio, texturaPiedraOto);
    BloquesOtoSP7 = new THREE.Mesh(BloquesOtonio, texturaPiedraOto);
    BloquesOtoSP8 = new THREE.Mesh(BloquesOtonio, texturaPiedraOto);


    BloquesOtoEstrellaA1 = new THREE.Mesh(TronoOtonio, texturaTronoOtoVer);
    BloquesOtoEstrellaA2 = new THREE.Mesh(TronoOtonio, texturaTronoOtoVer);
    BloquesOtoEstrellaA3 = new THREE.Mesh(TronoOtonio, texturaTronoOtoVer);
    BloquesOtoEstrellaB1 = new THREE.Mesh(TronoOtonio, texturaTronoOtoVer);
    BloquesOtoEstrellaB2 = new THREE.Mesh(TronoOtonio, texturaTronoOtoVer);
    BloquesOtoEstrellaB3 = new THREE.Mesh(TronoOtonio, texturaTronoOtoVer);
    BloquesOtoEstrellaC1 = new THREE.Mesh(TronoOtonio, texturaTronoOtoVer);
    BloquesOtoEstrellaC2 = new THREE.Mesh(TronoOtonio, texturaTronoOtoVer);
    BloquesOtoEstrellaC3 = new THREE.Mesh(TronoOtonio, texturaTronoOtoVer);
    BloquesOtoEstrellaD1 = new THREE.Mesh(TronoOtonio, texturaTronoOtoVer);
    BloquesOtoEstrellaD2 = new THREE.Mesh(TronoOtonio, texturaTronoOtoVer);
    BloquesOtoEstrellaD3 = new THREE.Mesh(TronoOtonio, texturaTronoOtoVer);

    ElevadorSP = new THREE.Mesh(PlataformaOtonioSP, texturPlataformaOto2);
    ElevadorNivel1 = new THREE.Mesh(PlataformaOtonio, texturPlataformaOto1);
    ElevadorNivel2 = new THREE.Mesh(PlataformaOtonio, texturPlataformaOto1);

    //POSICIONES 
    OtonioB.position.y -= 1300;
    // ---Linea 1---
    BloquesOto1.position.y -= 1185;
    BloquesOto1.position.x -= 920;
    BloquesOto1.position.z = 766;

    BloquesOto2.position.y -= 1185;
    BloquesOto2.position.x -= 612;
    BloquesOto2.position.z = 766;

    BloquesOto3.position.y -= 1185;
    BloquesOto3.position.x -= 458;
    BloquesOto3.position.z = 766;

    BloquesOto4.position.y -= 1185;
    BloquesOto4.position.x = 4;
    BloquesOto4.position.z = 766;

    BloquesOto5.position.y -= 1185;
    BloquesOto5.position.x = 466;
    BloquesOto5.position.z = 766;

    BloquesOto6.position.y -= 1185;
    BloquesOto6.position.x = 620;
    BloquesOto6.position.z = 766;

    // ---Linea 2---
    BloquesOto7.position.y -= 1185;
    BloquesOto7.position.x -= 766;
    BloquesOto7.position.z = 612;

    BloquesOto8.position.y -= 1185;
    BloquesOto8.position.x -= 458;
    BloquesOto8.position.z = 612;

    BloquesOto9.position.y -= 1185;
    BloquesOto9.position.x = 158;
    BloquesOto9.position.z = 612;

    BloquesOto10.position.y -= 1185;
    BloquesOto10.position.x = 312;
    BloquesOto10.position.z = 612;

    BloquesOto11.position.y -= 1185;
    BloquesOto11.position.x = 466;
    BloquesOto11.position.z = 612;

    // ---Linea 3---
    BloquesOto12.position.y -= 1185;
    BloquesOto12.position.x -= 612;
    BloquesOto12.position.z = 458;

    BloquesOto13.position.y -= 1185;
    BloquesOto13.position.x -= 458;
    BloquesOto13.position.z = 458;

    BloquesOto14.position.y -= 1185;
    BloquesOto14.position.x -= 304;
    BloquesOto14.position.z = 458;

    BloquesOto15.position.y -= 1185;
    BloquesOto15.position.x = 158;
    BloquesOto15.position.z = 458;

    BloquesOto16.position.y -= 1185;
    BloquesOto16.position.x = 466;
    BloquesOto16.position.z = 458;

    BloquesOto17.position.y -= 1185;
    BloquesOto17.position.x = 620;
    BloquesOto17.position.z = 458;

    BloquesOto18.position.y -= 1185;
    BloquesOto18.position.x = 774;
    BloquesOto18.position.z = 458;

    // ---Linea 4---
    BloquesOto19.position.y -= 1185;
    BloquesOto19.position.x -= 458;
    BloquesOto19.position.z = 304;

    BloquesOto20.position.y -= 1185;
    BloquesOto20.position.x -= 150;
    BloquesOto20.position.z = 304;

    BloquesOto21.position.y -= 1185;
    BloquesOto21.position.x = 158;
    BloquesOto21.position.z = 304;

    BloquesOto22.position.y -= 1185;
    BloquesOto22.position.x = 466;
    BloquesOto22.position.z = 304;

    // ---Linea 5---
    BloquesOto23.position.y -= 1185;
    BloquesOto23.position.x -= 766;
    BloquesOto23.position.z = 150;

    BloquesOto24.position.y -= 1185;
    BloquesOto24.position.x -= 458;
    BloquesOto24.position.z = 150;

    BloquesOto25.position.y -= 1185;
    BloquesOto25.position.x -= 304;
    BloquesOto25.position.z = 150;

    BloquesOto26.position.y -= 1185;
    BloquesOto26.position.x = 774;
    BloquesOto26.position.z = 150;

    // ---Linea 6---

    BloquesOto27.position.y -= 1185;
    BloquesOto27.position.x -= 612;
    BloquesOto27.position.z -= 4;

    BloquesOto28.position.y -= 1185;
    BloquesOto28.position.x = 466;
    BloquesOto28.position.z -= 4;

    BloquesOto29.position.y -= 1185;
    BloquesOto29.position.x = 774;
    BloquesOto29.position.z -= 4;

    // ---Linea 7---
    BloquesOto30.position.y -= 1185;
    BloquesOto30.position.x -= 458;
    BloquesOto30.position.z -= 150;

    BloquesOto31.position.y -= 1185;
    BloquesOto31.position.x = 312;
    BloquesOto31.position.z -= 150;

    BloquesOto32.position.y -= 1185;
    BloquesOto32.position.x = 620;
    BloquesOto32.position.z -= 150;

    // ---Linea 8---
    BloquesOto33.position.y -= 1185;
    BloquesOto33.position.x -= 150;
    BloquesOto33.position.z -= 304;

    BloquesOto34.position.y -= 1185;
    BloquesOto34.position.x = 4;
    BloquesOto34.position.z -= 304;

    BloquesOto35.position.y -= 1185;
    BloquesOto35.position.x = 158;
    BloquesOto35.position.z -= 304;

    BloquesOto36.position.y -= 1185;
    BloquesOto36.position.x = 312;
    BloquesOto36.position.z -= 304;

    BloquesOto37.position.y -= 1185;
    BloquesOto37.position.x = 774;
    BloquesOto37.position.z -= 304;

    // ---Linea 9---
    BloquesOto38.position.y -= 1185;
    BloquesOto38.position.x -= 766;
    BloquesOto38.position.z -= 458;

    BloquesOto39.position.y -= 1185;
    BloquesOto39.position.x -= 150;
    BloquesOto39.position.z -= 458;

    BloquesOto40.position.y -= 1185;
    BloquesOto40.position.x = 466;
    BloquesOto40.position.z -= 458;

    BloquesOto41.position.y -= 1185;
    BloquesOto41.position.x = 774;
    BloquesOto41.position.z -= 458;

    // ---Linea 10---
    BloquesOto42.position.y -= 1185;
    BloquesOto42.position.x -= 766;
    BloquesOto42.position.z -= 612;

    //-------------Segundo piso
    BloquesOto43.position.y -= 1185;
    BloquesOto43.position.x -= 612;
    BloquesOto43.position.z -= 612;

    BloquesOtoSP1.position.y -= 1045;
    BloquesOtoSP1.position.x -= 612;
    BloquesOtoSP1.position.z -= 612;

    BloquesOtoSP2.position.y -= 1045;
    BloquesOtoSP2.position.x -= 458;
    BloquesOtoSP2.position.z -= 612;

    BloquesOtoSP3.position.y -= 1045;
    BloquesOtoSP3.position.x -= 304;
    BloquesOtoSP3.position.z -= 612;

    BloquesOtoSP4.position.y -= 1045;
    BloquesOtoSP4.position.x -= 150;
    BloquesOtoSP4.position.z -= 612;

    BloquesOto44.position.y -= 1185;
    BloquesOto44.position.x -= 150;
    BloquesOto44.position.z -= 612;
    //------------------------------    

    BloquesOto45.position.y -= 1185;
    BloquesOto45.position.x = 158;
    BloquesOto45.position.z -= 612;

    BloquesOto46.position.y -= 1185;
    BloquesOto46.position.x = 312;
    BloquesOto46.position.z -= 612;

    BloquesOto47.position.y -= 1185;
    BloquesOto47.position.x = 466;
    BloquesOto47.position.z -= 612;

    BloquesOto48.position.y -= 1185;
    BloquesOto48.position.x = 620;
    BloquesOto48.position.z -= 612;

    BloquesOto49.position.y -= 1185;
    BloquesOto49.position.x = 774;
    BloquesOto49.position.z -= 612;

    // ---Linea 11---

    //-------Segundo piso---------
    BloquesOto50.position.y -= 1185;
    BloquesOto50.position.x = -612;
    BloquesOto50.position.z -= 776;

    BloquesOtoSP5.position.y -= 1045;
    BloquesOtoSP5.position.x -= 612;
    BloquesOtoSP5.position.z -= 776;

    BloquesOtoSP6.position.y -= 1045;
    BloquesOtoSP6.position.x -= 458;
    BloquesOtoSP6.position.z -= 776;

    BloquesOtoSP7.position.y -= 1045;
    BloquesOtoSP7.position.x -= 304;
    BloquesOtoSP7.position.z -= 776;

    BloquesOtoSP8.position.y -= 1045;
    BloquesOtoSP8.position.x -= 150;
    BloquesOtoSP8.position.z -= 776;

    BloquesOto51.position.y -= 1185;
    BloquesOto51.position.x -= 150;
    BloquesOto51.position.z -= 776;
    //------------------------------

    BloquesOto52.position.y -= 1185;
    BloquesOto52.position.x = 4;
    BloquesOto52.position.z -= 776;

    BloquesOto53.position.y -= 1185;
    BloquesOto53.position.x = 158;
    BloquesOto53.position.z -= 776;

    BloquesOto54.position.y -= 1185;
    BloquesOto54.position.x = 466;
    BloquesOto54.position.z -= 776;

    BloquesOto55.position.y -= 1185;
    BloquesOto55.position.x = 928;
    BloquesOto55.position.z -= 776;

    // ---Tronos---
    BloquesOtoEstrellaA1.position.y -= 1185;
    BloquesOtoEstrellaA1.position.x = 312;
    BloquesOtoEstrellaA1.position.z = 458;

    BloquesOtoEstrellaA2.position.y -= 1045;
    BloquesOtoEstrellaA2.position.x = 312;
    BloquesOtoEstrellaA2.position.z = 458;

    BloquesOtoEstrellaA3.position.y -= 905;
    BloquesOtoEstrellaA3.position.x = 312;
    BloquesOtoEstrellaA3.position.z = 458;

    BloquesOtoEstrellaB1.position.y -= 1185;
    BloquesOtoEstrellaB1.position.x = 466;
    BloquesOtoEstrellaB1.position.z -= 150;

    BloquesOtoEstrellaB2.position.y -= 1045;
    BloquesOtoEstrellaB2.position.x = 466;
    BloquesOtoEstrellaB2.position.z -= 150;

    BloquesOtoEstrellaB3.position.y -= 905;
    BloquesOtoEstrellaB3.position.x = 466;
    BloquesOtoEstrellaB3.position.z -= 150;

    BloquesOtoEstrellaC1.position.y -= 1185;
    BloquesOtoEstrellaC1.position.x -= 766;
    BloquesOtoEstrellaC1.position.z -= 150;

    BloquesOtoEstrellaC2.position.y -= 1045;
    BloquesOtoEstrellaC2.position.x -= 766;
    BloquesOtoEstrellaC2.position.z -= 150;

    BloquesOtoEstrellaC3.position.y -= 905;
    BloquesOtoEstrellaC3.position.x -= 766;
    BloquesOtoEstrellaC3.position.z -= 150;

    BloquesOtoEstrellaD1.position.y -= 1185;
    BloquesOtoEstrellaD1.position.x = 774;
    BloquesOtoEstrellaD1.position.z -= 776;

    BloquesOtoEstrellaD2.position.y -= 1045;
    BloquesOtoEstrellaD2.position.x = 774;
    BloquesOtoEstrellaD2.position.z -= 776;

    BloquesOtoEstrellaD3.position.y -= 905;
    BloquesOtoEstrellaD3.position.x = 774;
    BloquesOtoEstrellaD3.position.z -= 776;

    //-------------Elevadores------------
    ElevadorSP.position.y -= 1240;
    ElevadorSP.position.x -= 458;
    ElevadorSP.position.z -= 458;

    ElevadorNivel1.position.y -= 1240;
    ElevadorNivel1.position.x -= 150;
    ElevadorNivel1.position.z -= 4;

    ElevadorNivel2.position.y -= 1240;
    ElevadorNivel2.position.x = 158;
    ElevadorNivel2.position.z -= 4;



  /*  scene.add(OtonioB, BloquesOto1, BloquesOto2, BloquesOto3, BloquesOto4, BloquesOto5, BloquesOto6, BloquesOto7,
        BloquesOto8, BloquesOto9, BloquesOto10, BloquesOto11, BloquesOto12, BloquesOto13, BloquesOto14, BloquesOto15,
        BloquesOto16, BloquesOto17, BloquesOto18, BloquesOto19, BloquesOto20, BloquesOto21, BloquesOto22, BloquesOto23,
        BloquesOto24, BloquesOto25, BloquesOto26, BloquesOto27, BloquesOto28, BloquesOto29, BloquesOto30, BloquesOto31,
        BloquesOto32, BloquesOto33, BloquesOto34, BloquesOto35, BloquesOto36, BloquesOto37, BloquesOto38, BloquesOto39,
        BloquesOto40, BloquesOto41, BloquesOto42, BloquesOto43, BloquesOto44, BloquesOto45, BloquesOto46, BloquesOto47,
        BloquesOto48, BloquesOto49, BloquesOto50, BloquesOto51, BloquesOto52, BloquesOto53, BloquesOto54, BloquesOto55,
        BloquesOtoEstrellaA1, BloquesOtoEstrellaA2, BloquesOtoEstrellaA3, BloquesOtoEstrellaB1, BloquesOtoEstrellaB2,
        BloquesOtoEstrellaB3, BloquesOtoEstrellaC1, BloquesOtoEstrellaC2, BloquesOtoEstrellaC3, BloquesOtoEstrellaD1,
        BloquesOtoEstrellaD2, BloquesOtoEstrellaD3, BloquesOtoSP1, BloquesOtoSP2, BloquesOtoSP3, BloquesOtoSP4, BloquesOtoSP5,
        BloquesOtoSP6, BloquesOtoSP7, BloquesOtoSP8, ElevadorSP, ElevadorNivel1, ElevadorNivel2);
/*
    /*------------------------------------------------------ INVIERNO -------------------------------------------------*/

    InviernoB = new THREE.Mesh(SueloPInvierno, texturaInvSuelo);
    BloquesInv1 = new THREE.Mesh(BloquesInvierno, texturaPiedraInv);

    InviernoB.position.y -= 2600;


    /*--------------------------------------------------------------------------------------*/
    BloquesInv1 = new THREE.Mesh(BloquesInvierno);
    BloquesInv2 = new THREE.Mesh(BloquesHielo, texturaHielo);
    BloquesInv3 = new THREE.Mesh(BloquesInvierno, texturaPiedraInv);
    BloquesInv4 = new THREE.Mesh(BloquesHielo, texturaHielo);
    BloquesInv5 = new THREE.Mesh(BloquesHielo, texturaHielo);
    BloquesInv6 = new THREE.Mesh(BloquesInvierno, texturaPiedraInv);
    BloquesInv7 = new THREE.Mesh(BloquesHielo, texturaHielo);
    BloquesInv8 = new THREE.Mesh(BloquesInvierno, texturaPiedraInv);
    BloquesInv9 = new THREE.Mesh(BloquesHielo, texturaHielo);
    BloquesInv10 = new THREE.Mesh(BloquesHielo, texturaHielo);
    BloquesInv11 = new THREE.Mesh(BloquesHielo, texturaHielo);
    BloquesInv12 = new THREE.Mesh(BloquesHielo, texturaHielo);
    BloquesInv13 = new THREE.Mesh(BloquesInvierno, texturaPiedraInv);
    BloquesInv14 = new THREE.Mesh(BloquesHielo, texturaHielo);
    BloquesInv15 = new THREE.Mesh(BloquesInvierno, texturaPiedraInv);
    BloquesInv16 = new THREE.Mesh(BloquesHielo, texturaHielo);
    BloquesInv17 = new THREE.Mesh(BloquesHielo, texturaHielo);
    BloquesInv18 = new THREE.Mesh(BloquesHielo, texturaHielo);
    BloquesInv19 = new THREE.Mesh(BloquesHielo, texturaHielo);
    BloquesInv20 = new THREE.Mesh(BloquesInvierno, texturaPiedraInv);
    BloquesInv21 = new THREE.Mesh(BloquesInvierno, texturaPiedraInv);
    BloquesInv22 = new THREE.Mesh(BloquesHielo, texturaHielo);
    BloquesInv23 = new THREE.Mesh(BloquesHielo, texturaHielo);
    BloquesInv24 = new THREE.Mesh(BloquesHielo, texturaHielo);
    BloquesInv25 = new THREE.Mesh(BloquesHielo, texturaHielo);
    BloquesInv26 = new THREE.Mesh(BloquesInvierno, texturaPiedraInv);
    BloquesInv27 = new THREE.Mesh(BloquesHielo, texturaHielo);
    BloquesInv28 = new THREE.Mesh(BloquesHielo, texturaHielo);
    BloquesInv29 = new THREE.Mesh(BloquesHielo, texturaHielo);
    BloquesInv30 = new THREE.Mesh(BloquesHielo, texturaHielo);
    BloquesInv31 = new THREE.Mesh(BloquesHielo, texturaHielo);
    BloquesInv32 = new THREE.Mesh(BloquesHielo, texturaHielo);
    BloquesInv33 = new THREE.Mesh(BloquesHielo, texturaHielo);
    BloquesInv34 = new THREE.Mesh(BloquesHielo, texturaHielo);
    BloquesInv35 = new THREE.Mesh(BloquesInvierno, texturaPiedraInv);
    BloquesInv36 = new THREE.Mesh(BloquesHielo, texturaHielo);
    BloquesInv37 = new THREE.Mesh(BloquesHielo, texturaHielo);
    BloquesInv38 = new THREE.Mesh(BloquesHielo, texturaHielo);
    BloquesInv39 = new THREE.Mesh(BloquesHielo, texturaHielo);
    BloquesInv40 = new THREE.Mesh(BloquesHielo, texturaHielo);
    BloquesInv41 = new THREE.Mesh(BloquesInvierno, texturaPiedraInv);
    BloquesInv42 = new THREE.Mesh(BloquesInvierno, texturaPiedraInv);
    BloquesInv43 = new THREE.Mesh(BloquesHielo, texturaHielo);
    BloquesInv44 = new THREE.Mesh(BloquesHielo, texturaHielo);
    BloquesInv45 = new THREE.Mesh(BloquesInvierno, texturaPiedraInv);
    BloquesInv46 = new THREE.Mesh(BloquesHielo, texturaHielo);
    BloquesInv47 = new THREE.Mesh(BloquesInvierno, texturaPiedraInv);
    BloquesInv48 = new THREE.Mesh(BloquesHielo, texturaHielo);
    BloquesInv49 = new THREE.Mesh(BloquesHielo, texturaHielo);
    BloquesInv50 = new THREE.Mesh(BloquesInvierno, texturaPiedraInv);
    BloquesInv51 = new THREE.Mesh(BloquesHielo, texturaHielo);
    BloquesInv52 = new THREE.Mesh(BloquesHielo, texturaHielo);
    BloquesInv53 = new THREE.Mesh(BloquesHielo, texturaHielo);
    BloquesInv54 = new THREE.Mesh(BloquesHielo, texturaHielo);
    BloquesInv55 = new THREE.Mesh(BloquesHielo, texturaHielo);
    BloquesInv56 = new THREE.Mesh(BloquesHielo, texturaHielo);
    BloquesInv57 = new THREE.Mesh(BloquesHielo, texturaHielo);
    BloquesInv58 = new THREE.Mesh(BloquesInvierno, texturaPiedraInv);
    BloquesInv59 = new THREE.Mesh(BloquesInvierno, texturaPiedraInv);
    BloquesInv60 = new THREE.Mesh(BloquesInvierno, texturaPiedraInv);
    BloquesInv61 = new THREE.Mesh(BloquesHielo, texturaHielo);
    BloquesInv62 = new THREE.Mesh(BloquesHielo, texturaHielo);
    BloquesInv63 = new THREE.Mesh(BloquesInvierno, texturaPiedraInv);
    BloquesInv64 = new THREE.Mesh(BloquesHielo, texturaHielo);
    BloquesInv65 = new THREE.Mesh(BloquesInvierno, texturaPiedraInv);
    BloquesInv66 = new THREE.Mesh(BloquesHielo, texturaHielo);
    BloquesInv67 = new THREE.Mesh(BloquesHielo, texturaHielo);
    BloquesInv68 = new THREE.Mesh(BloquesHielo, texturaHielo);
    BloquesInv69 = new THREE.Mesh(BloquesHielo, texturaHielo);
    BloquesInv70 = new THREE.Mesh(BloquesInvierno, texturaPiedraInv);
    BloquesInv71 = new THREE.Mesh(BloquesHielo, texturaHielo);
    BloquesInv72 = new THREE.Mesh(BloquesHielo, texturaHielo);
    BloquesInv73 = new THREE.Mesh(BloquesHielo, texturaHielo);
    BloquesInv74 = new THREE.Mesh(BloquesInvierno, texturaPiedraInv);
    BloquesInv75 = new THREE.Mesh(BloquesHielo, texturaHielo);
    BloquesInv76 = new THREE.Mesh(BloquesInvierno, texturaPiedraInv);
    BloquesInv77 = new THREE.Mesh(BloquesInvierno, texturaPiedraInv);
    BloquesInv78 = new THREE.Mesh(BloquesHielo, texturaHielo);
    BloquesInv79 = new THREE.Mesh(BloquesInvierno, texturaPiedraInv);
    BloquesInv80 = new THREE.Mesh(BloquesHielo, texturaHielo);
    BloquesInv81 = new THREE.Mesh(BloquesHielo, texturaHielo);
    BloquesInv82 = new THREE.Mesh(BloquesInvierno, texturaPiedraInv);
    BloquesInv83 = new THREE.Mesh(BloquesHielo, texturaHielo);
    BloquesInv84 = new THREE.Mesh(BloquesInvierno, texturaPiedraInv);

    BloquesEstrellaA1 = new THREE.Mesh(TronoInvierno, texturaTronoInv);
    BloquesEstrellaA2 = new THREE.Mesh(TronoInvierno, texturaTronoInv);
    BloquesEstrellaA3 = new THREE.Mesh(TronoInvierno, texturaTronoInv);
    BloquesEstrellaB1 = new THREE.Mesh(TronoInvierno, texturaTronoInv);
    BloquesEstrellaB2 = new THREE.Mesh(TronoInvierno, texturaTronoInv);
    BloquesEstrellaB3 = new THREE.Mesh(TronoInvierno, texturaTronoInv);
    BloquesEstrellaC1 = new THREE.Mesh(TronoInvierno, texturaTronoInv);
    BloquesEstrellaC2 = new THREE.Mesh(TronoInvierno, texturaTronoInv);
    BloquesEstrellaC3 = new THREE.Mesh(TronoInvierno, texturaTronoInv);
    BloquesEstrellaD1 = new THREE.Mesh(TronoInvierno, texturaTronoInv);
    BloquesEstrellaD2 = new THREE.Mesh(TronoInvierno, texturaTronoInv);
    BloquesEstrellaD3 = new THREE.Mesh(TronoInvierno, texturaTronoInv);
    BloquesEstrellaE1 = new THREE.Mesh(TronoInvierno, texturaTronoInv);
    BloquesEstrellaE2 = new THREE.Mesh(TronoInvierno, texturaTronoInv);
    BloquesEstrellaE3 = new THREE.Mesh(TronoInvierno, texturaTronoInv);
    BloquesEstrellaF1 = new THREE.Mesh(TronoInvierno, texturaTronoInv);
    BloquesEstrellaF2 = new THREE.Mesh(TronoInvierno, texturaTronoInv);
    BloquesEstrellaF3 = new THREE.Mesh(TronoInvierno, texturaTronoInv);

    ElevadorInvNivel1 = new THREE.Mesh(PlataformaInvierno, texturPlataformaInv);
    ElevadorInvNivel2 = new THREE.Mesh(PlataformaInvierno, texturPlataformaInv);
    ElevadorInvNivel3 = new THREE.Mesh(PlataformaInvierno, texturPlataformaInv);

    //POSICIONES 
    // ---Linea 1---
    BloquesInv1.position.y = -2433;
    BloquesInv1.position.x -= 1417;
    BloquesInv1.position.z = 1417;

    BloquesInv2.position.y = -2433;
    BloquesInv2.position.x -= 914;
    BloquesInv2.position.z = 1081;

    BloquesInv3.position.y = -2433;
    BloquesInv3.position.x -= 1081;
    BloquesInv3.position.z = 1081;

    BloquesInv4.position.y = -2433;
    BloquesInv4.position.x -= 914;
    BloquesInv4.position.z = 747;

    BloquesInv5.position.y = -2433;
    BloquesInv5.position.x -= 1081;
    BloquesInv5.position.z = 914;

    BloquesInv6.position.y = -2433;
    BloquesInv6.position.x -= 1081;
    BloquesInv6.position.z = 747;

    BloquesInv7.position.y = -2433;
    BloquesInv7.position.x -= 1248;
    BloquesInv7.position.z = 747;

    BloquesInv8.position.y = -2433;
    BloquesInv8.position.x -= 747;
    BloquesInv8.position.z = 747;

    BloquesInv9.position.y = -2433;
    BloquesInv9.position.x -= 1250;
    BloquesInv9.position.z = 580;

    BloquesInv10.position.y = -2433;
    BloquesInv10.position.x -= 914;
    BloquesInv10.position.z = 580;

    BloquesInv11.position.y = -2433;
    BloquesInv11.position.x -= 1081;
    BloquesInv11.position.z = 580;

    BloquesInv12.position.y = -2433;
    BloquesInv12.position.x -= 1250;
    BloquesInv12.position.z = 413;

    BloquesInv13.position.y = -2433;
    BloquesInv13.position.x -= 1081;
    BloquesInv13.position.z = 413;

    BloquesInv14.position.y = -2433;
    BloquesInv14.position.x -= 914;
    BloquesInv14.position.z = 413;

    BloquesInv15.position.y = -2433;
    BloquesInv15.position.x -= 747;
    BloquesInv15.position.z = 413;

    BloquesInv16.position.y = -2433;
    BloquesInv16.position.x -= 1081;
    BloquesInv16.position.z = 246;

    BloquesInv17.position.y = -2433;
    BloquesInv17.position.x -= 914;
    BloquesInv17.position.z = 246;

    BloquesInv18.position.y = -2433;
    BloquesInv18.position.x -= 747;
    BloquesInv18.position.z = 246;

    BloquesInv19.position.y = -2433;
    BloquesInv19.position.x -= 914;
    BloquesInv19.position.z = 79;

    BloquesInv20.position.y = -2433;
    BloquesInv20.position.x -= 1250;
    BloquesInv20.position.z -= 88;

    BloquesInv21.position.y = -2433;
    BloquesInv21.position.x -= 1250;
    BloquesInv21.position.z -= 255;

    BloquesInv22.position.y = -2433;
    BloquesInv22.position.x -= 1081;
    BloquesInv22.position.z -= 255;

    BloquesInv23.position.y = -2433;
    BloquesInv23.position.x -= 1250;
    BloquesInv23.position.z -= 422;

    BloquesInv24.position.y = -2433;
    BloquesInv24.position.x -= 914;
    BloquesInv24.position.z -= 589;

    BloquesInv25.position.y = -2433;
    BloquesInv25.position.x -= 747;
    BloquesInv25.position.z -= 589;

    BloquesInv26.position.y = -2433;
    BloquesInv26.position.x -= 1081;
    BloquesInv26.position.z -= 756;

    BloquesInv27.position.y = -2433;
    BloquesInv27.position.x -= 914;
    BloquesInv27.position.z -= 756;

    BloquesInv28.position.y = -2433;
    BloquesInv28.position.x -= 747;
    BloquesInv28.position.z -= 756;

    BloquesInv29.position.y = -2433;
    BloquesInv29.position.x -= 1250;
    BloquesInv29.position.z -= 923;

    BloquesInv30.position.y = -2433;
    BloquesInv30.position.x -= 914;
    BloquesInv30.position.z -= 923;

    BloquesInv31.position.y = -2433;
    BloquesInv31.position.x -= 580;
    BloquesInv31.position.z -= 923;

    BloquesInv32.position.y = -2433;
    BloquesInv32.position.x -= 914;
    BloquesInv32.position.z -= 1090;

    BloquesInv33.position.y = -2433;
    BloquesInv33.position.x -= 747;
    BloquesInv33.position.z -= 1090;

    BloquesInv34.position.y = -2433;
    BloquesInv34.position.x -= 580;
    BloquesInv34.position.z = 1081;

    BloquesInv35.position.y = -2433;
    BloquesInv35.position.x -= 413;
    BloquesInv35.position.z = 1081;

    BloquesInv36.position.y = -2433;
    BloquesInv36.position.x -= 246;
    BloquesInv36.position.z = 914;

    BloquesInv37.position.y = -2433;
    BloquesInv37.position.x -= 79;
    BloquesInv37.position.z = 914;

    BloquesInv38.position.y = -2433;
    BloquesInv38.position.x = 88;
    BloquesInv38.position.z = 914;

    BloquesInv39.position.y = -2433;
    BloquesInv39.position.x = 88;
    BloquesInv39.position.z = 1081;

    BloquesInv40.position.y = -2433;
    BloquesInv40.position.x = 88;
    BloquesInv40.position.z = 1250;

    BloquesInv41.position.y = -2433;
    BloquesInv41.position.x = 255;
    BloquesInv41.position.z = 914;

    BloquesInv42.position.y = -2433;
    BloquesInv42.position.x = 255;
    BloquesInv42.position.z = 1250;

    BloquesInv43.position.y = -2433;
    BloquesInv43.position.x -= 79;
    BloquesInv43.position.z = 580;

    BloquesInv44.position.y = -2433;
    BloquesInv44.position.x -= 246;
    BloquesInv44.position.z = 246;

    BloquesInv45.position.y = -2433;
    BloquesInv45.position.x = 88;
    BloquesInv45.position.z = 246;

    BloquesInv46.position.y = -2433;
    BloquesInv46.position.x = 88;
    BloquesInv46.position.z = 79;

    BloquesInv47.position.y = -2433;
    BloquesInv47.position.x -= 413;
    BloquesInv47.position.z -= 88;

    BloquesInv48.position.y = -2433;
    BloquesInv48.position.x = 255;
    BloquesInv48.position.z -= 88;

    BloquesInv49.position.y = -2433;
    BloquesInv49.position.x = 88;
    BloquesInv49.position.z = -246;

    BloquesInv50.position.y = -2433;
    BloquesInv50.position.x -= 79;
    BloquesInv50.position.z = -413;

    BloquesInv51.position.y = -2433;
    BloquesInv51.position.x -= 246;
    BloquesInv51.position.z = -413;

    BloquesInv52.position.y = -2433;
    BloquesInv52.position.x -= 246;
    BloquesInv52.position.z = -747;

    BloquesInv53.position.y = -2433;
    BloquesInv53.position.x = 88;
    BloquesInv53.position.z = -747;

    BloquesInv54.position.y = -2433;
    BloquesInv54.position.x = 88;
    BloquesInv54.position.z = -914;

    BloquesInv55.position.y = -2433;
    BloquesInv55.position.x = 255;
    BloquesInv55.position.z = -914;

    BloquesInv56.position.y = -2433;
    BloquesInv56.position.x = 422;
    BloquesInv56.position.z = 914;

    BloquesInv57.position.y = -2433;
    BloquesInv57.position.x = 422;
    BloquesInv57.position.z = 1250;

    BloquesInv58.position.y = -2433;
    BloquesInv58.position.x = 589;
    BloquesInv58.position.z = 1081;

    BloquesInv59.position.y = -2433;
    BloquesInv59.position.x = 589;
    BloquesInv59.position.z = 914;

    BloquesInv60.position.y = -2433;
    BloquesInv60.position.x = 589;
    BloquesInv60.position.z = 747;

    BloquesInv61.position.y = -2433;
    BloquesInv61.position.x = 923;
    BloquesInv61.position.z = 1250;

    BloquesInv62.position.y = -2433;
    BloquesInv62.position.x = 923;
    BloquesInv62.position.z = 1081;
    //----------------
    BloquesInv63.position.y = -2433;
    BloquesInv63.position.x = 1090;
    BloquesInv63.position.z = 1250;

    BloquesInv64.position.y = -2433;
    BloquesInv64.position.x = 1090;
    BloquesInv64.position.z = 747;

    BloquesInv65.position.y = -2433;
    BloquesInv65.position.x = 1090;
    BloquesInv65.position.z = 580;

    BloquesInv66.position.y = -2433;
    BloquesInv66.position.x = 923;
    BloquesInv66.position.z = 580;

    BloquesInv67.position.y = -2433;
    BloquesInv67.position.x = 589;
    BloquesInv67.position.z = 413;

    BloquesInv68.position.y = -2433;
    BloquesInv68.position.x = 589;
    BloquesInv68.position.z = 246;

    BloquesInv69.position.y = -2433;
    BloquesInv69.position.x = 756;
    BloquesInv69.position.z = 79;

    BloquesInv70.position.y = -2433;
    BloquesInv70.position.x = 923;
    BloquesInv70.position.z = 246;

    BloquesInv71.position.y = -2433;
    BloquesInv71.position.x = 1090;
    BloquesInv71.position.z = 246;

    BloquesInv72.position.y = -2433;
    BloquesInv72.position.x = 926;
    BloquesInv72.position.z = 79;

    BloquesInv73.position.y = -2433;
    BloquesInv73.position.x = 1090;
    BloquesInv73.position.z = 79;

    BloquesInv74.position.y = -2433;
    BloquesInv74.position.x = 923;
    BloquesInv74.position.z = -88;

    BloquesInv75.position.y = -2433;
    BloquesInv75.position.x = 756;
    BloquesInv75.position.z = -255;

    BloquesInv76.position.y = -2433;
    BloquesInv76.position.x = 589;
    BloquesInv76.position.z = -422;

    BloquesInv77.position.y = -2433;
    BloquesInv77.position.x = 589;
    BloquesInv77.position.z = -756;

    BloquesInv78.position.y = -2433;
    BloquesInv78.position.x = 756;
    BloquesInv78.position.z = -756;

    BloquesInv79.position.y = -2433;
    BloquesInv79.position.x = 923;
    BloquesInv79.position.z = -756;
    //----------------------------
    BloquesInv80.position.y = -2433;
    BloquesInv80.position.x = 589;
    BloquesInv80.position.z = -923;

    BloquesInv81.position.y = -2433;
    BloquesInv81.position.x = 923;
    BloquesInv81.position.z = -923;
    //------------------------------
    BloquesInv82.position.y = -2433;
    BloquesInv82.position.x = 589;
    BloquesInv82.position.z = -1090;

    BloquesInv83.position.y = -2433;
    BloquesInv83.position.x = 756;
    BloquesInv83.position.z = -1090;

    BloquesInv84.position.y = -2433;
    BloquesInv84.position.x = 923;
    BloquesInv84.position.z = -1090;


    //----------------TRONOS-------------
    BloquesEstrellaA1.position.y = -2433;
    BloquesEstrellaA1.position.x -= 1250;
    BloquesEstrellaA1.position.z = 246;

    BloquesEstrellaA2.position.y = -2273;
    BloquesEstrellaA2.position.x -= 1250;
    BloquesEstrellaA2.position.z = 246;

    BloquesEstrellaA3.position.y = -2113;
    BloquesEstrellaA3.position.x -= 1250;
    BloquesEstrellaA3.position.z = 246;

    BloquesEstrellaB1.position.y = -2433;
    BloquesEstrellaB1.position.x -= 747;
    BloquesEstrellaB1.position.z -= 923;

    BloquesEstrellaB2.position.y = -2273;
    BloquesEstrellaB2.position.x -= 747;
    BloquesEstrellaB2.position.z -= 923;

    BloquesEstrellaB3.position.y = -2113;
    BloquesEstrellaB3.position.x -= 747;
    BloquesEstrellaB3.position.z -= 923;

    BloquesEstrellaC1.position.y = -2433;
    BloquesEstrellaC1.position.x = 88;
    BloquesEstrellaC1.position.z -= 413;

    BloquesEstrellaC2.position.y = -2273;
    BloquesEstrellaC2.position.x = 88;
    BloquesEstrellaC2.position.z -= 413;

    BloquesEstrellaC3.position.y = -2113;
    BloquesEstrellaC3.position.x = 88;
    BloquesEstrellaC3.position.z -= 413;

    BloquesEstrellaD1.position.y = -2433;
    BloquesEstrellaD1.position.x = 422;
    BloquesEstrellaD1.position.z = 1081;

    BloquesEstrellaD2.position.y = -2273;
    BloquesEstrellaD2.position.x = 422;
    BloquesEstrellaD2.position.z = 1081;

    BloquesEstrellaD3.position.y = -2113;
    BloquesEstrellaD3.position.x = 422;
    BloquesEstrellaD3.position.z = 1081;

    BloquesEstrellaE1.position.y = -2433;
    BloquesEstrellaE1.position.x = 756;
    BloquesEstrellaE1.position.z = 246;

    BloquesEstrellaE2.position.y = -2273;
    BloquesEstrellaE2.position.x = 756;
    BloquesEstrellaE2.position.z = 246;

    BloquesEstrellaE3.position.y = -2113;
    BloquesEstrellaE3.position.x = 756;
    BloquesEstrellaE3.position.z = 246;

    BloquesEstrellaF1.position.y = -2433;
    BloquesEstrellaF1.position.x = 756;
    BloquesEstrellaF1.position.z = -923;

    BloquesEstrellaF2.position.y = -2273;
    BloquesEstrellaF2.position.x = 756;
    BloquesEstrellaF2.position.z = -923;

    BloquesEstrellaF3.position.y = -2113;
    BloquesEstrellaF3.position.x = 756;
    BloquesEstrellaF3.position.z = -923;

    // ----------- Plataformas
    ElevadorInvNivel1.position.y -= 2500;
    ElevadorInvNivel1.position.x -= 79;
    ElevadorInvNivel1.position.z -= 246;

    ElevadorInvNivel2.position.y -= 2500;
    ElevadorInvNivel2.position.x -= 79;
    ElevadorInvNivel2.position.z = 88;

    ElevadorInvNivel3.position.y -= 2500;
    ElevadorInvNivel3.position.x = 88;
    ElevadorInvNivel3.position.z -= 79;




    scene.add(InviernoB, BloquesInv2, BloquesInv3, BloquesInv4, BloquesInv5, BloquesInv6,
        BloquesInv7, BloquesInv8, BloquesInv9, BloquesInv10, BloquesInv11, BloquesInv12, BloquesInv13,
        BloquesInv14, BloquesInv15, BloquesInv16, BloquesInv17, BloquesInv18, BloquesInv19, BloquesInv20,
        BloquesInv21, BloquesInv22, BloquesInv23, BloquesInv24, BloquesInv25, BloquesInv26, BloquesInv27,
        BloquesInv28, BloquesInv29, BloquesInv30, BloquesInv31, BloquesInv32, BloquesInv33, BloquesEstrellaA1,
        BloquesEstrellaB1, BloquesEstrellaC1, BloquesEstrellaD1, BloquesEstrellaE1, BloquesInv34, BloquesInv35,
        BloquesInv36, BloquesInv37, BloquesInv38, BloquesInv39, BloquesInv40, BloquesInv41, BloquesInv42, BloquesInv43,
        BloquesInv44, BloquesInv45, BloquesInv46, BloquesInv47, BloquesInv48, BloquesInv49, BloquesInv50, BloquesInv51,
        BloquesInv52, BloquesInv53, BloquesInv54, BloquesInv55, BloquesInv56, BloquesInv57, BloquesInv58, BloquesInv59,
        BloquesInv60, BloquesInv61, BloquesInv62, BloquesInv63, BloquesInv64, BloquesInv65, BloquesInv66, BloquesInv67,
        BloquesInv68, BloquesInv69, BloquesInv70, BloquesInv71, BloquesInv72, BloquesInv73, BloquesInv74, BloquesInv75,
        BloquesInv76, BloquesInv77, BloquesInv78, BloquesInv79, BloquesInv80, BloquesInv81, BloquesInv82, BloquesInv83,
        BloquesInv84, BloquesEstrellaF1, BloquesEstrellaA2, BloquesEstrellaA3, BloquesEstrellaB2, BloquesEstrellaB3, BloquesEstrellaC2,
        BloquesEstrellaC3, BloquesEstrellaD2, BloquesEstrellaD3, BloquesEstrellaE2, BloquesEstrellaE3, BloquesEstrellaF2,
        BloquesEstrellaF3, ElevadorInvNivel1, ElevadorInvNivel2, ElevadorInvNivel3);


    // --------------------- ESTRELLAS Y POWER UPS  --------------------
    /*
    Primavera estrellas y power ups
    */
    EstrellaPrim = new THREE.Mesh(BloquesPrimavera, texturaEstrella);
    VelocidadPrim = new THREE.Mesh(BloquesPrimavera, texturaEstrella);

    EstrellaPrim.position.y = 580;
    EstrellaPrim.position.x -= 62.66;
    EstrellaPrim.position.z -= 62;

    /*
   Otonio estrellas y power ups
   */
    EstrellaOto1 = new THREE.Mesh(BloquesOtonio, texturaEstrella);
    EstrellaOto2 = new THREE.Mesh(BloquesOtonio, texturaEstrella);
    EstrellaOto3 = new THREE.Mesh(BloquesOtonio, texturaEstrella);
    EstrellaOto4 = new THREE.Mesh(BloquesOtonio, texturaEstrella);

    EstrellaOto1.position.y -= 765;
    EstrellaOto1.position.x = 312;
    EstrellaOto1.position.z = 458;

    EstrellaOto2.position.y -= 765;
    EstrellaOto2.position.x = 466;
    EstrellaOto2.position.z -= 150;

    EstrellaOto3.position.y -= 765;
    EstrellaOto3.position.x -= 766;
    EstrellaOto3.position.z -= 150;

    EstrellaOto4.position.y -= 765;
    EstrellaOto4.position.x = 774;
    EstrellaOto4.position.z -= 776;

    /*
   Invierno estrellas y power ups
   */

    EstrellaInv1 = new THREE.Mesh(BloquesInvierno, texturaEstrella);
    EstrellaInv2 = new THREE.Mesh(BloquesInvierno, texturaEstrella);
    EstrellaInv3 = new THREE.Mesh(BloquesInvierno, texturaEstrella);
    EstrellaInv4 = new THREE.Mesh(BloquesInvierno, texturaEstrella);
    EstrellaInv5 = new THREE.Mesh(BloquesInvierno, texturaEstrella);
    EstrellaInv6 = new THREE.Mesh(BloquesInvierno, texturaEstrella);

    VelocidadPU1 = new THREE.Mesh(PUInv, texturaVelocidadInvierno);
    VelocidadPU2 = new THREE.Mesh(PUInv, texturaVelocidadInvierno);
    VelocidadPU3 = new THREE.Mesh(PUInv, texturaVelocidadInvierno);
    PUVelocityList[0] = new Velocity(VelocidadPU1, 50);
    PUVelocityList[1] = new Velocity(VelocidadPU2, 50);
    PUVelocityList[2] = new Velocity(VelocidadPU3, 50);

    InvBombaPU3 = new THREE.Mesh(PUInv, texturaBombasInv);
    InvBombaPU2 = new THREE.Mesh(PUInv, texturaBombasInv);
    InvBombaPU1 = new THREE.Mesh(PUInv, texturaBombasInv);
    PUPotencyList[0] = new Potency(InvBombaPU1, 50);
    PUPotencyList[1] = new Potency(InvBombaPU2, 50);
    PUPotencyList[2] = new Potency(InvBombaPU3, 50);

    InvVidaPU1 = new THREE.Mesh(PUInv, texturaVida);
    InvVidaPU2 = new THREE.Mesh(PUInv, texturaVida);
    InvVidaPU3 = new THREE.Mesh(PUInv, texturaVida);
    PUImmunityList[0] = new Immunity(InvVidaPU1, 50);
    PUImmunityList[1] = new Immunity(InvVidaPU2, 50);
    PUImmunityList[2] = new Immunity(InvVidaPU3, 50);

    EstrellaInv1.position.y = -1950;
    EstrellaInv1.position.x -= 1250;
    EstrellaInv1.position.z = 246;

    EstrellaInv2.position.y = -1950;
    EstrellaInv2.position.x -= 747;
    EstrellaInv2.position.z -= 923;

    EstrellaInv3.position.y = -1950;
    EstrellaInv3.position.x = 88;
    EstrellaInv3.position.z -= 413;

    EstrellaInv4.position.y = -1950;
    EstrellaInv4.position.x = 422;
    EstrellaInv4.position.z = 1081;

    EstrellaInv5.position.y = -1950;
    EstrellaInv5.position.x = 756;
    EstrellaInv5.position.z = 246;

    EstrellaInv6.position.y = -1950;
    EstrellaInv6.position.x = 756;
    EstrellaInv6.position.z = -923;

    VelocidadPU1.position.y = -2440;
    VelocidadPU1.position.x -= 914;
    VelocidadPU1.position.z = 1081;

    VelocidadPU2.position.y = -2440;
    VelocidadPU2.position.x = 255;
    VelocidadPU2.position.z = -914;

    VelocidadPU3.position.y = -2440;
    VelocidadPU3.position.x -= 1250;
    VelocidadPU3.position.z -= 923;

    InvBombaPU1.position.y = -2440;
    InvBombaPU1.position.x -= 246;
    InvBombaPU1.position.z = 914;

    InvBombaPU2.position.y = -2440;
    InvBombaPU2.position.x = 589;
    InvBombaPU2.position.z = 246;

    InvBombaPU3.position.y = -2440;
    InvBombaPU3.position.x -= 1248;
    InvBombaPU3.position.z = 747;

    InvVidaPU1.position.y = -2440;
    InvVidaPU1.position.x = 255;
    InvVidaPU1.position.z -= 88;

    InvVidaPU2.position.y = -2440;
    InvVidaPU2.position.x = 1090;
    InvVidaPU2.position.z = 246;

    InvVidaPU3.position.y = -2440;
    InvVidaPU3.position.x -= 1250;
    InvVidaPU3.position.z = 413;


 /*  scene.add(EstrellaPrim, EstrellaOto1, EstrellaOto2, EstrellaOto3, EstrellaOto4 
        );*/


scene.add(EstrellaInv1, EstrellaInv2, EstrellaInv3,EstrellaInv4, EstrellaInv5, EstrellaInv6, VelocidadPU1, VelocidadPU2, VelocidadPU3, InvBombaPU3, InvBombaPU2, InvBombaPU1, InvVidaPU1,
    InvVidaPU2, InvVidaPU3);

    //----------------------------------------------------------------------------

    collidableList.push(BloquesPrim1, BloquesPrim4, BloquesPrim5, BloquesPrim7, BloquesPrim10, BloquesPrim13, BloquesPrim15, BloquesPrim16
        , BloquesPrim18, BloquesPrim19, BloquesPrim21, BloquesOto3, BloquesOto4, BloquesOto5, BloquesOto6, BloquesOto7, BloquesOto10,
        BloquesOto12, BloquesOto14, BloquesOto17, BloquesOto22, BloquesOto23, BloquesOto25, BloquesOto27, BloquesOto28, BloquesOto31,
        BloquesOto34, BloquesOto36, BloquesOto37, BloquesOto41, BloquesOto43, BloquesOto44, BloquesOto46, BloquesOto48, BloquesOto50,
        BloquesOto51, BloquesOto53, BloquesOto55, BloquesOtoSP1, BloquesOtoSP2, BloquesOtoSP3, BloquesOtoSP4, BloquesOtoSP5, BloquesOtoSP6,
        BloquesOtoSP7, BloquesOtoSP8, BloquesInv3, BloquesInv6, BloquesInv8, BloquesInv13, BloquesInv15, BloquesInv20, BloquesInv21,
        BloquesInv26, BloquesInv35, BloquesInv41, BloquesInv42, BloquesInv45, BloquesInv47, BloquesInv50, BloquesInv58, BloquesInv59, BloquesInv60,
        BloquesInv63, BloquesInv65, BloquesInv70, BloquesInv74, BloquesInv76, BloquesInv77, BloquesInv79, BloquesInv82, BloquesInv84, InvPlano3,
        InvPlano1, InvPlano2, InvPlano4, PrimaveraPlano1, PrimaveraPlano2, PrimaveraPlano3, PrimaveraPlano4, OtoPlano1, OtoPlano2, OtoPlano3, OtoPlano4, BloquesInv2, BloquesInv4, BloquesInv5, BloquesInv7, BloquesInv9, BloquesInv10, BloquesInv11,
        BloquesInv12, BloquesInv14, BloquesInv16, BloquesInv17, BloquesInv18, BloquesInv19, BloquesInv22, BloquesInv23, BloquesInv24, BloquesInv25,
        BloquesInv27, BloquesInv28, BloquesInv29, BloquesInv30, BloquesInv31, BloquesInv32, BloquesInv33, BloquesInv34, BloquesInv36, BloquesInv37,
        BloquesInv38, BloquesInv39, BloquesInv40, BloquesInv43, BloquesInv44, BloquesInv46, BloquesInv48, BloquesInv49, BloquesInv51, BloquesInv52,
        BloquesInv53, BloquesInv54, BloquesInv55, BloquesInv56, BloquesInv57, BloquesInv61, BloquesInv62, BloquesInv64, BloquesInv66, BloquesInv67,
        BloquesInv68, BloquesInv69, BloquesInv71, BloquesInv72, BloquesInv73, BloquesInv75, BloquesInv78, BloquesInv80, BloquesInv81, BloquesInv83,
        BloquesPrim2, BloquesPrim3, BloquesPrim6, BloquesPrim8, BloquesPrim9, BloquesPrim11, BloquesPrim12, BloquesPrim14, BloquesPrim17, BloquesPrim20,
        BloquesPrim22, BloquesPrim23, BloquesEstrellaA1, BloquesEstrellaA2, BloquesEstrellaA3, BloquesEstrellaB1, BloquesEstrellaB2, BloquesEstrellaB3,
        BloquesEstrellaC1, BloquesEstrellaC2, BloquesEstrellaC3, BloquesEstrellaD1, BloquesEstrellaD2, BloquesEstrellaD3, BloquesEstrellaE1, BloquesOtoEstrellaA1,
        BloquesOtoEstrellaA2, BloquesOtoEstrellaA3, BloquesOtoEstrellaB1, BloquesOtoEstrellaB2, BloquesOtoEstrellaB3, BloquesOtoEstrellaC1, BloquesOtoEstrellaC2,
        BloquesOtoEstrellaC3, BloquesOtoEstrellaD1, BloquesOtoEstrellaD2, BloquesOtoEstrellaD3, BloquesEstrellaE2, BloquesEstrellaE3, BloquesEstrellaF1,
        BloquesEstrellaF2, BloquesEstrellaF3, BloquesPrimEstrella1, BloquesPrimEstrella2, BloquesPrimEstrella3, BloquesPrimEstrella4, BloquesPrimEstrella5,
        BloquesOto2, BloquesOto1, BloquesOto8, BloquesOto9, BloquesOto11, BloquesOto13, BloquesOto15, BloquesOto16, BloquesOto18, BloquesOto19, BloquesOto20,
        BloquesOto21, BloquesOto24, BloquesOto26, BloquesOto29, BloquesOto30, BloquesOto32, BloquesOto33, BloquesOto35, BloquesOto38, BloquesOto39, BloquesOto41,
        BloquesOto42, BloquesOto45, BloquesOto47, BloquesOto49, BloquesOto52, BloquesOto54);

    destruibleList.push(BloquesInv2, BloquesInv4, BloquesInv5, BloquesInv7, BloquesInv9, BloquesInv10, BloquesInv11,
        BloquesInv12, BloquesInv14, BloquesInv16, BloquesInv17, BloquesInv18, BloquesInv19, BloquesInv22, BloquesInv23, BloquesInv24, BloquesInv25,
        BloquesInv27, BloquesInv28, BloquesInv29, BloquesInv30, BloquesInv31, BloquesInv32, BloquesInv33, BloquesInv34, BloquesInv36, BloquesInv37,
        BloquesInv38, BloquesInv39, BloquesInv40, BloquesInv43, BloquesInv44, BloquesInv46, BloquesInv48, BloquesInv49, BloquesInv51, BloquesInv52,
        BloquesInv53, BloquesInv54, BloquesInv55, BloquesInv56, BloquesInv57, BloquesInv61, BloquesInv62, BloquesInv64, BloquesInv66, BloquesInv67,
        BloquesInv68, BloquesInv69, BloquesInv71, BloquesInv72, BloquesInv73, BloquesInv75, BloquesInv78, BloquesInv80, BloquesInv81, BloquesInv83,
        BloquesPrim2, BloquesPrim3, BloquesPrim6, BloquesPrim8, BloquesPrim9, BloquesPrim11, BloquesPrim12, BloquesPrim14, BloquesPrim17, BloquesPrim20,
        BloquesPrim22, BloquesPrim23, BloquesEstrellaA1, BloquesEstrellaA2, BloquesEstrellaA3, BloquesEstrellaB1, BloquesEstrellaB2, BloquesEstrellaB3,
        BloquesEstrellaC1, BloquesEstrellaC2, BloquesEstrellaC3, BloquesEstrellaD1, BloquesEstrellaD2, BloquesEstrellaD3, BloquesEstrellaE1, BloquesOtoEstrellaA1,
        BloquesOtoEstrellaA2, BloquesOtoEstrellaA3, BloquesOtoEstrellaB1, BloquesOtoEstrellaB2, BloquesOtoEstrellaB3, BloquesOtoEstrellaC1, BloquesOtoEstrellaC2,
        BloquesOtoEstrellaC3, BloquesOtoEstrellaD1, BloquesOtoEstrellaD2, BloquesOtoEstrellaD3, BloquesEstrellaE2, BloquesEstrellaE3, BloquesEstrellaF1,
        BloquesEstrellaF2, BloquesEstrellaF3, BloquesPrimEstrella1, BloquesPrimEstrella2, BloquesPrimEstrella3, BloquesPrimEstrella4, BloquesPrimEstrella5,
        BloquesOto2, BloquesOto1, BloquesOto8, BloquesOto9, BloquesOto11, BloquesOto13, BloquesOto15, BloquesOto16, BloquesOto18, BloquesOto19, BloquesOto20,
        BloquesOto21, BloquesOto24, BloquesOto26, BloquesOto29, BloquesOto30, BloquesOto32, BloquesOto33, BloquesOto35, BloquesOto38, BloquesOto39, BloquesOto41,
        BloquesOto42, BloquesOto45, BloquesOto47, BloquesOto49, BloquesOto52, BloquesOto54);






        cameras.invierno.target=InviernoB.position;
      /*  cameras.invierno.position.y = -1600;
        cameras.invierno.position.x = 1000;
        cameras.invierno.position.z -= 1000;
    */

}
/**
 * Function to render application over
 * and over.
 */
function animateScene() {
    requestAnimationFrame(animateScene);
    renderer.shadowMap.enabled = true;
    renderer.shadowMap.type = THREE.PCFSoftShadowMap;
    renderer.render(scene, cameras.current);
    updateScene();
}

function updateScene() {
    //Confirmaremos si hay un ganador
    if (players.length == 1) {
        gameOver();
    }

    //Updating camera view by control inputs
    cameraControl.update();


    //Updating destroyable things
    for (let i = 0; i < destroyableBox.length; i++) {
        if (destroyableBox[i] != null) {
            destroyableBox[i].update();
        }
    }

    //Updating velocity power up
    for (let i = 0; i < PUVelocityList.length; i++) {
        if (PUVelocityList[i] != null) {
            PUVelocityList[i].update();
        }
    }

    //Updating potency power up
    for (let i = 0; i < PUPotencyList.length; i++) {
        if (PUPotencyList[i] != null) {
            PUPotencyList[i].update();
        }
    }

    //Updating immunity power up
    for (let i = 0; i < PUImmunityList.length; i++) {
        if (PUImmunityList[i] != null) {
            PUImmunityList[i].update();
        }
    }

    //Updating stats
    //stats.update();

    //Players controls
    for (const player of Object.keys(players)) {
        if (players[player] != null) {
            players[player].updateControls();
            players[player].collidableBox.update(players[player].control);
        }
    }

    //Cameras

    cameras.personal.position.copy(players[0].element.position);
    cameras.personal.position.y += 300;
    cameras.personal.position.x += 300;
    cameras.personal.position.z -= 300;

}
function onWindowResize() {
    cameras.current.aspect = window.innerWidth / window.innerHeight;
    cameras.current.updateProjectionMatrix();
    renderer.setSize(window.innerWidth, window.innerHeight);
}